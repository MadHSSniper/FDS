package data.missions.fds_syndicate_strikes_back;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        //Setting up fleets
        //Both attacking
        api.initFleet(FleetSide.PLAYER, "FDS", FleetGoal.ATTACK, false);
        api.initFleet(FleetSide.ENEMY, "HSS", FleetGoal.ATTACK, true);

        // Set a small blurb for each fleet that shows up on the mission detail and
        // mission results screens to identify each side.
        api.setFleetTagline(FleetSide.PLAYER, "Syndicate Attack Fleet");
        api.setFleetTagline(FleetSide.ENEMY, "Hegemony Patrol Fleet");

        // These show up as items in the bulleted list under 
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Leave no survivors!");

        // Set up the player's fleet. Use "variant" names
        api.addToFleet(FleetSide.PLAYER, "fds_suffering_elite", FleetMemberType.SHIP, "Annihilator", true);
        api.addToFleet(FleetSide.PLAYER, "fds_hatred_assault", FleetMemberType.SHIP, "Destroyer", false);
        api.addToFleet(FleetSide.PLAYER, "fds_fear_assault", FleetMemberType.SHIP, "Conqueror", false);
        api.addToFleet(FleetSide.PLAYER, "fds_fear_assault", FleetMemberType.SHIP, "Victorious", false);
        api.addToFleet(FleetSide.PLAYER, "fds_fear_strike", FleetMemberType.SHIP, "Undefeated", false);
        api.addToFleet(FleetSide.PLAYER, "fds_revenge_standard", FleetMemberType.SHIP, "Vanguard", false);
        api.addToFleet(FleetSide.PLAYER, "fds_wrath_support", FleetMemberType.SHIP, "Infiltrator", false);
        api.addToFleet(FleetSide.PLAYER, "fds_wrath_elite", FleetMemberType.SHIP, "Righteous", false);
        api.addToFleet(FleetSide.PLAYER, "fds_atonement_support", FleetMemberType.SHIP, "Frost", false);
        api.addToFleet(FleetSide.PLAYER, "fds_atonement_assault", FleetMemberType.SHIP, "Ice", false);
        api.addToFleet(FleetSide.PLAYER, "fds_retaliation_standard", FleetMemberType.SHIP, "Avenger", false);
        api.addToFleet(FleetSide.PLAYER, "fds_pride_strike", FleetMemberType.SHIP, "Providence", false);
        api.addToFleet(FleetSide.PLAYER, "fds_affliction_assault", FleetMemberType.SHIP, "Afflictor", false);
        api.addToFleet(FleetSide.PLAYER, "fds_despair_standard", FleetMemberType.SHIP, "Desperate", false);
        api.addToFleet(FleetSide.PLAYER, "fds_despair_standard", FleetMemberType.SHIP, "Punisher", false);
        api.addToFleet(FleetSide.PLAYER, "fds_submission_standard", FleetMemberType.SHIP, "Barrage", false);
        api.addToFleet(FleetSide.PLAYER, "fds_grief_mk_ii_assault", FleetMemberType.SHIP, "Midas", false);
        api.addToFleet(FleetSide.PLAYER, "fds_grief_mk_ii_assault", FleetMemberType.SHIP, "Magnate", false);
        api.addToFleet(FleetSide.PLAYER, "fds_sorrow_elite", FleetMemberType.SHIP, "Fortuitous", false);
        api.addToFleet(FleetSide.PLAYER, "fds_sorrow_elite", FleetMemberType.SHIP, "Fair Trade", false);
        api.addToFleet(FleetSide.PLAYER, "fds_agony_assault", FleetMemberType.SHIP, "Big Break", false);
        api.addToFleet(FleetSide.PLAYER, "fds_agony_mk_ii_standard", FleetMemberType.SHIP, "Margin Call", false);
        api.addToFleet(FleetSide.PLAYER, "fds_disturbance_standard", FleetMemberType.SHIP, "Baron", false);


        api.addToFleet(FleetSide.ENEMY, "onslaught_Standard", FleetMemberType.SHIP, "Invulnerable", true);
        api.addToFleet(FleetSide.ENEMY, "onslaught_Standard", FleetMemberType.SHIP, "Unshakable", false);
        api.addToFleet(FleetSide.ENEMY, "onslaught_Outdated", FleetMemberType.SHIP, "Unconquerable", false);
        api.addToFleet(FleetSide.ENEMY, "dominator_Assault", FleetMemberType.SHIP, "Uprising", false);
        api.addToFleet(FleetSide.ENEMY, "dominator_Assault", FleetMemberType.SHIP, "Spartacus", false);
        api.addToFleet(FleetSide.ENEMY, "heron_Strike", FleetMemberType.SHIP, "Enterprise", false);
        api.addToFleet(FleetSide.ENEMY, "eagle_Balanced", FleetMemberType.SHIP, "Resolute", false);
        api.addToFleet(FleetSide.ENEMY, "eagle_Balanced", FleetMemberType.SHIP, "Faithful", false);
        api.addToFleet(FleetSide.ENEMY, "eagle_Assault", FleetMemberType.SHIP, "Devoted", false);
        api.addToFleet(FleetSide.ENEMY, "falcon_CS", FleetMemberType.SHIP, "Defiant", false);
        api.addToFleet(FleetSide.ENEMY, "falcon_Attack", FleetMemberType.SHIP, "Boxer", false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Balanced", FleetMemberType.SHIP, "Vicious", false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Support", FleetMemberType.SHIP, "Domitius", false);
        api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, "Pax", false);
        api.addToFleet(FleetSide.ENEMY, "enforcer_Balanced", FleetMemberType.SHIP, "Nox", false);
        api.addToFleet(FleetSide.ENEMY, "vigilance_FS", FleetMemberType.SHIP, "Fast", false);
        api.addToFleet(FleetSide.ENEMY, "vigilance_FS", FleetMemberType.SHIP, "Swift", false);
        api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, "Striker", false);
        api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, "Punisher", false);
        api.addToFleet(FleetSide.ENEMY, "lasher_PD", FleetMemberType.SHIP, "Terror", false);

        // Set up the map.
        // 12000x8000 is actually somewhat small, making for a faster-paced mission.
        float width = 15000f;
        float height = 15000f;
        api.initMap((float) -width / 2f, (float) width / 2f, (float) -height / 2f, (float) height / 2f);

        float minX = -width / 2;
        float minY = -height / 2;

        // All the addXXX methods take a pair of coordinates followed by data for
        // whatever object is being added.
        // Add two big nebula clouds
        api.addNebula(minX + width * 0.2f, minY + height * 0.6f, 1200);
        api.addNebula(minX + width * 0.3f, minY + height * 0.4f, 2000);
        api.addNebula(minX + width * 0.6f, minY + height * 0.7f, 1000);
        api.addNebula(minX + width * 0.5f, minY + height * 0.2f, 500);

        // And a few random ones to spice up the playing field.
        // A similar approach can be used to randomize everything
        // else, including fleet composition.
        for (int i = 0; i < 8; i++) {
            float x = (float) Math.random() * width - width / 2;
            float y = (float) Math.random() * height - height / 2;
            float radius = 100f + (float) Math.random() * 400f;
            api.addNebula(x, y, radius);
        }

        // Add objectives. These can be captured by each side
        // and provide stat bonuses and extra command points to
        // bring in reinforcements.
        // Reinforcements only matter for large fleets - in this
        // case, assuming a 100 command point battle size,
        // both fleets will be able to deploy fully right away.
        /*api.addObjective(minX + width * 0.75f, minY + height * 0.25f, "sensor_array");
        api.addObjective(minX + width * 0.75f, minY + height * 0.75f, "nav_buoy");
        api.addObjective(minX + width * 0.5f, minY + height * 0.6f, "comm_relay");
        api.addObjective(minX + width * 0.25f, minY + height * 0.75f, "sensor_array");
        api.addObjective(minX + width * 0.25f, minY + height * 0.25f, "nav_buoy");
         */
    }
}
