package data.missions.fds_revenge_of_the_syndicate;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        //Setting up fleets
        //Both attacking
        api.initFleet(FleetSide.PLAYER, "FDS", FleetGoal.ATTACK, false);
        api.initFleet(FleetSide.ENEMY, "HSS", FleetGoal.ATTACK, true);

        // Set a small blurb for each fleet that shows up on the mission detail and
        // mission results screens to identify each side.
        api.setFleetTagline(FleetSide.PLAYER, "Syndicate Strike Group");
        api.setFleetTagline(FleetSide.ENEMY, "Hegemony Assault Fleet");

        // These show up as items in the bulleted list under 
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Leave no survivors!");

        // Set up the player's fleet. Use "variant" names
        api.addToFleet(FleetSide.PLAYER, "fds_terror_assault", FleetMemberType.SHIP, "Syndicate's Horror", true);
        api.addToFleet(FleetSide.PLAYER, "fds_torture_assault", FleetMemberType.SHIP, "Syndicate's Might", false);
        api.addToFleet(FleetSide.PLAYER, "fds_hatred_assault", FleetMemberType.SHIP, "Suffering", false);
        api.addToFleet(FleetSide.PLAYER, "fds_suffering_strike", FleetMemberType.SHIP, "Twin Fury I", false);
        api.addToFleet(FleetSide.PLAYER, "fds_suffering_elite", FleetMemberType.SHIP, "Twin Fury II", false);
        api.addToFleet(FleetSide.PLAYER, "fds_hatred_assault", FleetMemberType.SHIP, "Hate", false);
        api.addToFleet(FleetSide.PLAYER, "fds_fear_standard", FleetMemberType.SHIP, "Anger", false);
        api.addToFleet(FleetSide.PLAYER, "fds_fear_standard", FleetMemberType.SHIP, "Fear", false);
        api.addToFleet(FleetSide.PLAYER, "fds_atonement_standard", FleetMemberType.SHIP, "Ravager", false);
        api.addToFleet(FleetSide.PLAYER, "fds_revenge_strike", FleetMemberType.SHIP, "No Quarter", false);
        api.addToFleet(FleetSide.PLAYER, "fds_wrath_elite", FleetMemberType.SHIP, "Bismark", false);
        api.addToFleet(FleetSide.PLAYER, "fds_rancour_elite", FleetMemberType.SHIP, "Luke", false);
        api.addToFleet(FleetSide.PLAYER, "fds_rancour_assault", FleetMemberType.SHIP, "Leia", false);
        api.addToFleet(FleetSide.PLAYER, "fds_affliction_assault", FleetMemberType.SHIP, "Council", false);
        api.addToFleet(FleetSide.PLAYER, "fds_agony_mk_ii_standard", FleetMemberType.SHIP, "Misery", false);
        api.addToFleet(FleetSide.PLAYER, "fds_agony_mk_ii_standard", FleetMemberType.SHIP, "Pain", false);
        api.addToFleet(FleetSide.PLAYER, "fds_agony_mk_ii_standard", FleetMemberType.SHIP, "Wound", false);
        api.addToFleet(FleetSide.PLAYER, "fds_sorrow_elite", FleetMemberType.SHIP, "Forgotten", false);
        api.addToFleet(FleetSide.PLAYER, "fds_disturbance_standard", FleetMemberType.SHIP, "Nightmare", false);
        api.addToFleet(FleetSide.PLAYER, "fds_disturbance_standard", FleetMemberType.SHIP, "Dream", false);
        api.addToFleet(FleetSide.PLAYER, "fds_grief_mk_ii_assault", FleetMemberType.SHIP, "Death", false);
        api.addToFleet(FleetSide.PLAYER, "fds_melancholy_mk_ii_assault", FleetMemberType.SHIP, "Guilt", false);


        api.addToFleet(FleetSide.ENEMY, "onslaught_Elite", FleetMemberType.SHIP, "Strength", true);
        api.addToFleet(FleetSide.ENEMY, "onslaught_Standard", FleetMemberType.SHIP, "Power", false);
        api.addToFleet(FleetSide.ENEMY, "legion_Escort", FleetMemberType.SHIP, "Legionnaire", false);
        api.addToFleet(FleetSide.ENEMY, "dominator_AntiCV", FleetMemberType.SHIP, "Bull", false);
        api.addToFleet(FleetSide.ENEMY, "eagle_Balanced", FleetMemberType.SHIP, "Unity", false);
        api.addToFleet(FleetSide.ENEMY, "eagle_Balanced", FleetMemberType.SHIP, "Union", false);
        api.addToFleet(FleetSide.ENEMY, "falcon_CS", FleetMemberType.SHIP, "Bird of Prey", false);
        api.addToFleet(FleetSide.ENEMY, "mora_Support", FleetMemberType.SHIP, "Meteor", false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Elite", FleetMemberType.SHIP, "Shark", false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Elite", FleetMemberType.SHIP, "Killer Whale", false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Balanced", FleetMemberType.SHIP, "Hammer", false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Balanced", FleetMemberType.SHIP, "Nail", false);
        api.addToFleet(FleetSide.ENEMY, "sunder_Assault", FleetMemberType.SHIP, "Lasher", false);
        api.addToFleet(FleetSide.ENEMY, "sunder_CS", FleetMemberType.SHIP, "Cutter", false);
        api.addToFleet(FleetSide.ENEMY, "wolf_hegemony_Assault", FleetMemberType.SHIP, "Pax", false);
        api.addToFleet(FleetSide.ENEMY, "wolf_hegemony_Assault", FleetMemberType.SHIP, "Nox", false);
        api.addToFleet(FleetSide.ENEMY, "lasher_Strike", FleetMemberType.SHIP, "Slash", false);
        api.addToFleet(FleetSide.ENEMY, "lasher_Strike", FleetMemberType.SHIP, "Maul", false);
        api.addToFleet(FleetSide.ENEMY, "lasher_Strike", FleetMemberType.SHIP, "Grind", false);
        api.addToFleet(FleetSide.ENEMY, "hound_hegemony_Standard", FleetMemberType.SHIP, "Bulldog", false);
        api.addToFleet(FleetSide.ENEMY, "hound_hegemony_Standard", FleetMemberType.SHIP, "Pitbull", false);

        // Set up the map.
        // 12000x8000 is actually somewhat small, making for a faster-paced mission.
        float width = 15000f;
        float height = 15000f;
        api.initMap((float) -width / 2f, (float) width / 2f, (float) -height / 2f, (float) height / 2f);

        float minX = -width / 2;
        float minY = -height / 2;

        // All the addXXX methods take a pair of coordinates followed by data for
        // whatever object is being added.
        // Add two big nebula clouds
        api.addNebula(minX + width * 0.2f, minY + height * 0.6f, 1200);
        api.addNebula(minX + width * 0.3f, minY + height * 0.4f, 2000);
        api.addNebula(minX + width * 0.6f, minY + height * 0.7f, 1000);
        api.addNebula(minX + width * 0.5f, minY + height * 0.2f, 500);

        // And a few random ones to spice up the playing field.
        // A similar approach can be used to randomize everything
        // else, including fleet composition.
        for (int i = 0; i < 8; i++) {
            float x = (float) Math.random() * width - width / 2;
            float y = (float) Math.random() * height - height / 2;
            float radius = 100f + (float) Math.random() * 400f;
            api.addNebula(x, y, radius);
        }

        // Add objectives. These can be captured by each side
        // and provide stat bonuses and extra command points to
        // bring in reinforcements.
        // Reinforcements only matter for large fleets - in this
        // case, assuming a 100 command point battle size,
        // both fleets will be able to deploy fully right away.
        /*api.addObjective(minX + width * 0.75f, minY + height * 0.25f, "sensor_array");
        api.addObjective(minX + width * 0.75f, minY + height * 0.75f, "nav_buoy");
        api.addObjective(minX + width * 0.5f, minY + height * 0.6f, "comm_relay");
        api.addObjective(minX + width * 0.25f, minY + height * 0.75f, "sensor_array");
        api.addObjective(minX + width * 0.25f, minY + height * 0.25f, "nav_buoy");
         */
    }
}
