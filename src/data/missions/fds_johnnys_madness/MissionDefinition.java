package data.missions.fds_johnnys_madness;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        //Setting up fleets
        //One attacking and other defending
        api.initFleet(FleetSide.PLAYER, "HSS", FleetGoal.ATTACK, false);
        api.initFleet(FleetSide.ENEMY, "FDS", FleetGoal.ATTACK, true);

        // Set a small blurb for each fleet that shows up on the mission detail and
        // mission results screens to identify each side.
        api.setFleetTagline(FleetSide.PLAYER, "Hegemony Revenge Fleet");
        api.setFleetTagline(FleetSide.ENEMY, "Syndicate High Admiral's Fleet");

        // These show up as items in the bulleted list under 
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Good luck!");

        // Set up the player's fleet. Use "variant" names
        api.addToFleet(FleetSide.PLAYER, "onslaught_Elite", FleetMemberType.SHIP, "Strength", true);
        api.addToFleet(FleetSide.PLAYER, "onslaught_Standard", FleetMemberType.SHIP, "Power", false);
        api.addToFleet(FleetSide.PLAYER, "onslaught_Standard", FleetMemberType.SHIP, "Invulnerable", false);
        api.addToFleet(FleetSide.PLAYER, "onslaught_Standard", FleetMemberType.SHIP, "Unshakable", false);
        api.addToFleet(FleetSide.PLAYER, "onslaught_Outdated", FleetMemberType.SHIP, "Unconquerable", false);
        api.addToFleet(FleetSide.PLAYER, "legion_Escort", FleetMemberType.SHIP, "Legionnaire", false);
        api.addToFleet(FleetSide.PLAYER, "mora_Support", FleetMemberType.SHIP, "Meteor", false);
        api.addToFleet(FleetSide.PLAYER, "dominator_Assault", FleetMemberType.SHIP, "Uprising", false);
        api.addToFleet(FleetSide.PLAYER, "dominator_Assault", FleetMemberType.SHIP, "Spartacus", false);
        api.addToFleet(FleetSide.PLAYER, "dominator_AntiCV", FleetMemberType.SHIP, "Bull", false);
        api.addToFleet(FleetSide.PLAYER, "heron_Attack", FleetMemberType.SHIP, "Valiant", false);
        api.addToFleet(FleetSide.PLAYER, "heron_Strike", FleetMemberType.SHIP, "Enterprise", false);
        api.addToFleet(FleetSide.PLAYER, "eagle_Balanced", FleetMemberType.SHIP, "Resolute", false);
        api.addToFleet(FleetSide.PLAYER, "eagle_Balanced", FleetMemberType.SHIP, "Unity", false);
        api.addToFleet(FleetSide.PLAYER, "eagle_Balanced", FleetMemberType.SHIP, "Union", false);
        api.addToFleet(FleetSide.PLAYER, "eagle_Balanced", FleetMemberType.SHIP, "Faithful", false);
        api.addToFleet(FleetSide.PLAYER, "eagle_Assault", FleetMemberType.SHIP, "Devoted", false);
        api.addToFleet(FleetSide.PLAYER, "falcon_xiv_Elite", FleetMemberType.SHIP, "Hunter", false);
        api.addToFleet(FleetSide.PLAYER, "falcon_CS", FleetMemberType.SHIP, "Defiant", false);
        api.addToFleet(FleetSide.PLAYER, "falcon_CS", FleetMemberType.SHIP, "Bird of Prey", false);
        api.addToFleet(FleetSide.PLAYER, "falcon_Attack", FleetMemberType.SHIP, "Boxer", false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Elite", FleetMemberType.SHIP, "Shark", false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Elite", FleetMemberType.SHIP, "Killer Whale", false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Balanced", FleetMemberType.SHIP, "Hammer", false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Balanced", FleetMemberType.SHIP, "Nail", false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Balanced", FleetMemberType.SHIP, "Vicious", false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Support", FleetMemberType.SHIP, "Domitius", false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Balanced", FleetMemberType.SHIP, "Hammer", false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Support", FleetMemberType.SHIP, "Anvil", false);
        api.addToFleet(FleetSide.PLAYER, "sunder_Assault", FleetMemberType.SHIP, "Piercer", false);
        api.addToFleet(FleetSide.PLAYER, "sunder_Assault", FleetMemberType.SHIP, "Lasher", false);
        api.addToFleet(FleetSide.PLAYER, "sunder_CS", FleetMemberType.SHIP, "Cutter", false);
        api.addToFleet(FleetSide.PLAYER, "enforcer_Balanced", FleetMemberType.SHIP, "Executioner", false);
        api.addToFleet(FleetSide.PLAYER, "enforcer_Assault", FleetMemberType.SHIP, "Pax", false);
        api.addToFleet(FleetSide.PLAYER, "enforcer_Balanced", FleetMemberType.SHIP, "Nox", false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_FS", FleetMemberType.SHIP, "Fast", false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_FS", FleetMemberType.SHIP, "Swift", false);
        api.addToFleet(FleetSide.PLAYER, "lasher_CS", FleetMemberType.SHIP, "Striker", false);
        api.addToFleet(FleetSide.PLAYER, "lasher_CS", FleetMemberType.SHIP, "Punisher", false);
        api.addToFleet(FleetSide.PLAYER, "lasher_PD", FleetMemberType.SHIP, "Terror", false);
        api.addToFleet(FleetSide.PLAYER, "wolf_hegemony_Assault", FleetMemberType.SHIP, "Pax", false);
        api.addToFleet(FleetSide.PLAYER, "wolf_hegemony_Assault", FleetMemberType.SHIP, "Nox", false);
        api.addToFleet(FleetSide.PLAYER, "lasher_Strike", FleetMemberType.SHIP, "Slash", false);
        api.addToFleet(FleetSide.PLAYER, "lasher_Strike", FleetMemberType.SHIP, "Maul", false);
        api.addToFleet(FleetSide.PLAYER, "lasher_Strike", FleetMemberType.SHIP, "Grind", false);
        api.addToFleet(FleetSide.PLAYER, "hound_hegemony_Standard", FleetMemberType.SHIP, "Bulldog", false);
        api.addToFleet(FleetSide.PLAYER, "hound_hegemony_Standard", FleetMemberType.SHIP, "Pitbull", false);
        api.addToFleet(FleetSide.PLAYER, "wolf_hegemony_Assault", FleetMemberType.SHIP, "Pack leader", false);
        api.addToFleet(FleetSide.PLAYER, "lasher_Strike", FleetMemberType.SHIP, "Slasher", false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_Strike", FleetMemberType.SHIP, "Guardian", false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_Strike", FleetMemberType.SHIP, "Watchman", false);


        api.addToFleet(FleetSide.ENEMY, "fds_wroth_standard", FleetMemberType.SHIP, "Mad Man Johnny", true);
//        api.addToFleet(FleetSide.ENEMY, "fds_terror_elite", FleetMemberType.SHIP, "Terror I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_terror_elite", FleetMemberType.SHIP, "Terror II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_torture_elite", FleetMemberType.SHIP, "Torture I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_torture_elite", FleetMemberType.SHIP, "Torture II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_torture_elite", FleetMemberType.SHIP, "Torture III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_suffering_elite", FleetMemberType.SHIP, "Suffering I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_suffering_elite", FleetMemberType.SHIP, "Suffering II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_suffering_elite", FleetMemberType.SHIP, "Suffering III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_suffering_elite", FleetMemberType.SHIP, "Suffering IV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_suffering_elite", FleetMemberType.SHIP, "Suffering V", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_hatred_elite", FleetMemberType.SHIP, "Hatred I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_hatred_elite", FleetMemberType.SHIP, "Hatred II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_hatred_elite", FleetMemberType.SHIP, "Hatred III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_hatred_elite", FleetMemberType.SHIP, "Hatred IV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_hatred_elite", FleetMemberType.SHIP, "Hatred V", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_atonement_elite", FleetMemberType.SHIP, "Atonement I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_atonement_elite", FleetMemberType.SHIP, "Atonement II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_atonement_elite", FleetMemberType.SHIP, "Atonement III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear IV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear V", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear VI", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear VII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear VIII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear IX", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_fear_elite", FleetMemberType.SHIP, "Fear X", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation IV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation V", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation VI", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation VII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation VIII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation IX", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_retaliation_elite", FleetMemberType.SHIP, "Retaliation X", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction IV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction V", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction VI", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction VII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction VIII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction IX", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_affliction_elite", FleetMemberType.SHIP, "Affliction X", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance IV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance V", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance VI", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance VII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance VIII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance IX", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_disturbance_elite", FleetMemberType.SHIP, "Disturbance X", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy I", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy II", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy III", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy IV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy V", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy VI", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy VII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy VIII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy IX", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy X", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XI", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XIII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XIV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XV", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XVI", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XVII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XVIII", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XIX", false);
//        api.addToFleet(FleetSide.ENEMY, "fds_melancholy_elite", FleetMemberType.SHIP, "Melancholy XX", false);

        // Set up the map.
        // 12000x8000 is actually somewhat small, making for a faster-paced mission.
        float width = 10000f;
        float height = 15000f;
        api.initMap((float) -width / 2f, (float) width / 2f, (float) -height / 2f, (float) height / 2f);
        api.addAsteroidField(0, 0, 0, width, 10, 100, 1000);

        float minX = -width / 2;
        float minY = -height / 2;

        // All the addXXX methods take a pair of coordinates followed by data for
        // whatever object is being added.
        // Add two big nebula clouds
        api.addNebula(width * 0.5f, height * 0.5f, 20000);

        // Add objectives. These can be captured by each side
        // and provide stat bonuses and extra command points to
        // bring in reinforcements.
        // Reinforcements only matter for large fleets - in this
        // case, assuming a 100 command point battle size,
        // both fleets will be able to deploy fully right away.
        /*api.addObjective(minX + width * 0.75f, minY + height * 0.25f, "sensor_array");
        api.addObjective(minX + width * 0.75f, minY + height * 0.75f, "nav_buoy");
        api.addObjective(minX + width * 0.5f, minY + height * 0.6f, "comm_relay");
        api.addObjective(minX + width * 0.25f, minY + height * 0.75f, "sensor_array");
        api.addObjective(minX + width * 0.25f, minY + height * 0.25f, "nav_buoy");
         */
    }
}
