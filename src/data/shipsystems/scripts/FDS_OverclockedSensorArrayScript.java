package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatFleetManagerAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import data.scripts.campaign.ids.FDS_IDs;
import data.scripts.plugins.FDS_SpriteRenderManager;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_OverclockedSensorArrayScript extends BaseShipSystemScript {

    public static final float MAX_RANGE = 7500f;
    private static final float RECOIL_BONUS = 0.9f; //10% decrease
    private static final float RANGE_BONUS = 1.1f; //10% increase
    private static final String SYSTEM_LOOP = "system_dampener_loop";
    private static final Color EFFECT_COLOR = new Color(255, 0, 0, 255);

    private boolean pointGiven = false;

    public static final float TIMER = 1.5f;
    private boolean add = true;
    private float level = 0f;

    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        CombatEngineAPI engine = Global.getCombatEngine();
        ShipAPI ship = (ShipAPI)stats.getEntity();

        if (ship != null) {
//            Vector2f loc = ship.getLocation();
//            engine.getFogOfWar(ally ? 0 : 1).revealAroundPoint(engine, loc.x, loc.y, MAX_RANGE);

            CombatFleetManagerAPI fleetManager = engine.getFleetManager(ship.getOwner());

            if (fleetManager != null && !pointGiven) {
                fleetManager.getTaskManager(true).getCommandPointsStat().modifyFlat(id, 1f);
                fleetManager.getTaskManager(false).getCommandPointsStat().modifyFlat(id, 1f);
                pointGiven = true;
            }

            Global.getSoundPlayer().playLoop(SYSTEM_LOOP, ship, 1f, 0.2f, ship.getLocation(), ship.getVelocity());

            if (add) {
                level += engine.getElapsedInLastFrame();
            } else {
                level -= engine.getElapsedInLastFrame();
            }

            if (level < 0f) {
                level = 0f;
                add = true;
            } else if (level > TIMER) {
                level = TIMER;
                add = false;
            }
            float opacity = 255 * (level/TIMER);

            FDS_SpriteRenderManager.singleFrameRender(
                    Global.getSettings().getSprite("misc", "fds_overclocked_sensor"),
                    ship.getLocation(),
                    new Vector2f(1000f, 1000f),
                    ship.getFacing(),
                    new Color(255, 0, 0, (int)opacity),
                    true
            );

            // Buff all allies in the map
            this.applyBuff(ship);

            List<ShipAPI> ships = AIUtils.getAlliesOnMap(ship);
            Iterator i$ = ships.iterator();

            ShipAPI target;
            while(i$.hasNext()) {
                target = (ShipAPI)i$.next();
                //if (target.getVariant().hasHullMod("fds_augmented_sensor_arrays")) {
                    this.applyBuff(target);
                //}
            }
        }
    }

    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship = (ShipAPI)stats.getEntity();
        this.unapplyBuff(ship);

        List<ShipAPI> ships = AIUtils.getAlliesOnMap(ship);
        Iterator i$ = ships.iterator();

        ShipAPI target;
        while(i$.hasNext()) {
            target = (ShipAPI)i$.next();
            this.unapplyBuff(target);
        }

        pointGiven = false;
    }

    private void applyBuff(ShipAPI target) {
        // Disable zero flux speed bonus
        MutableShipStatsAPI stats = target.getMutableStats();

        stats.getSensorStrength().modifyPercent(FDS_IDs.BUFF_OVERCLOCKED_SENSORS, 1f);
        stats.getMaxRecoilMult().modifyMult(FDS_IDs.BUFF_OVERCLOCKED_SENSORS, RECOIL_BONUS);
        stats.getBallisticWeaponRangeBonus().modifyMult(FDS_IDs.BUFF_OVERCLOCKED_SENSORS, RANGE_BONUS);
        stats.getEnergyWeaponRangeBonus().modifyMult(FDS_IDs.BUFF_OVERCLOCKED_SENSORS, RANGE_BONUS);
        stats.getBeamWeaponRangeBonus().modifyMult(FDS_IDs.BUFF_OVERCLOCKED_SENSORS, RANGE_BONUS);
    }

    private void unapplyBuff(ShipAPI target) {
        // Re-enable zero flux speed bonus
        MutableShipStatsAPI stats = target.getMutableStats();
        stats.getSensorStrength().unmodify(FDS_IDs.BUFF_OVERCLOCKED_SENSORS);
        stats.getMaxRecoilMult().unmodify(FDS_IDs.BUFF_OVERCLOCKED_SENSORS);
        stats.getBallisticWeaponRangeBonus().unmodify(FDS_IDs.BUFF_OVERCLOCKED_SENSORS);
        stats.getEnergyWeaponRangeBonus().unmodify(FDS_IDs.BUFF_OVERCLOCKED_SENSORS);
        stats.getBeamWeaponRangeBonus().unmodify(FDS_IDs.BUFF_OVERCLOCKED_SENSORS);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("sensor range increased", false);
        }

        return null;
    }
}
