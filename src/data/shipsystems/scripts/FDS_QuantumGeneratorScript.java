package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.dark.shaders.util.ShaderLib;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static data.scripts.campaign.ids.FDS_IDs.BUFF_SINGULARITY_GENERATOR;
import static data.scripts.campaign.ids.FDS_IDs.DEBUFF_SIGNATURE_DAMPENER;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_QuantumGeneratorScript extends BaseShipSystemScript {

    private static final String SYSTEM_EXIT = "system_quantum_generator_exit";
    private static final String SYSTEM_LIGHTNING = "system_emp_emitter_impact";

    private float timer = 0f;
    private float timeCounter = 0f;
    private boolean isActive = false;
    private boolean destSet = false;
    private boolean soundOn = false;
    private Color bluish = new Color(131, 205, 255);

    private Vector2f destination = new Vector2f(0f, 0f);
    private Vector2f originalDest = new Vector2f(0f, 0f);
    private Vector2f velocityVector = new Vector2f(0f, 0f);
    private Vector2f shiftedPosition = new Vector2f(0f, 0f);
    private Vector2f entryPoint = new Vector2f(0f, 0f);
    private float maxSpeed = 0f;
    private float startSpeed = 0f;
//    private RippleDistortion wave;

    private static Map arcRadius = new HashMap();
    static {
        arcRadius.put(ShipAPI.HullSize.FIGHTER, 25f);
        arcRadius.put(ShipAPI.HullSize.FRIGATE, 50f);
        arcRadius.put(ShipAPI.HullSize.DESTROYER, 100f);
        arcRadius.put(ShipAPI.HullSize.CRUISER, 150f);
        arcRadius.put(ShipAPI.HullSize.CAPITAL_SHIP, 250f);
    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        // instanceof also acts as a null check
        if (!(stats.getEntity() instanceof ShipAPI)) { return; }

        ShipAPI ship = (ShipAPI) stats.getEntity();
        CombatEngineAPI engine = Global.getCombatEngine();
        if (state == State.OUT) { /********** State.OUT **********/
            ship.getEngineController().fadeToOtherColor(this, Color.WHITE, null, 1f, 0.8f);
            ship.getEngineController().extendFlame(this, 5f, 0.25f, 1f);
            ship.giveCommand(ShipCommand.ACCELERATE, null, 0);

//            stats.getMaxSpeed().modifyFlat(BUFF_SINGULARITY_GENERATOR, 2000f * (effectLevel / 2f));
//            stats.getAcceleration().modifyFlat(BUFF_SINGULARITY_GENERATOR, 20000f);
//            stats.getDeceleration().modifyFlat(BUFF_SINGULARITY_GENERATOR, 20000f);
            ship.setCollisionClass(CollisionClass.NONE);
            ship.setExtraAlphaMult(1);
            ship.setApplyExtraAlphaToEngines(true);


            /***** Exit speed management *****/
            if (isActive) {
                velocityVector = directionNormalized(ship);
                maxSpeed = ship.getMaxSpeed() + 2000f;
                startSpeed = ship.getMaxSpeed(); // ship.getVelocity().length();
                isActive = false;
                soundOn = true;

                stats.getAcceleration().modifyMult(BUFF_SINGULARITY_GENERATOR, 0f);
                stats.getMaxSpeed().modifyFlat(BUFF_SINGULARITY_GENERATOR, 2000f * (effectLevel / 2f));
            }
            Vector2f scaledVelocity = new Vector2f(0f, 0f);
            VectorUtils.resize(velocityVector, 2000f, scaledVelocity);
            ship.getVelocity().set(scaledVelocity);

//            wave = new RippleDistortion();
//            wave.setLocation(destination);
//            wave.setSize((float)arcRadius.get(ship.getHullSpec().getHullSize()) * 10 * (1 - effectLevel));
//            wave.setIntensity((float)arcRadius.get(ship.getHullSpec().getHullSize()) * effectLevel);
//            wave.fadeOutIntensity(0.25f);
//            DistortionShader.addDistortion(wave);
        } else if (state == State.IN) { /********** State.IN **********/
            if(!engine.isPaused()){
                timer += engine.getElapsedInLastFrame();
            }

            ship.getEngineController().fadeToOtherColor(this, Color.WHITE, null, 1f, 0.8f);
            ship.getEngineController().extendFlame(this, 5f, 0.25f, 1f);
            ship.giveCommand(ShipCommand.ACCELERATE, null, 0);

            if (timer >= 0.5f) {
                if (!isActive) {
                    velocityVector = directionNormalized(ship);
                    isActive = true;

                    stats.getAcceleration().modifyMult(BUFF_SINGULARITY_GENERATOR, 0f);
                    stats.getMaxSpeed().modifyFlat(BUFF_SINGULARITY_GENERATOR, 2000f);
                }
                Vector2f scaledVelocity = new Vector2f(0f, 0f);
                VectorUtils.resize(velocityVector, 2000f, scaledVelocity);
                ship.getVelocity().set(scaledVelocity);
//                stats.getMaxSpeed().modifyFlat(BUFF_SINGULARITY_GENERATOR, 2000f);
//                stats.getAcceleration().modifyFlat(BUFF_SINGULARITY_GENERATOR, 20000f);
                ship.setCollisionClass(CollisionClass.NONE);
                ship.setExtraAlphaMult(1 - ((effectLevel - 0.5f) / 0.5f));
                ship.setApplyExtraAlphaToEngines(true);
            }

            /***** Entry lightning *****/
            Vector2f vel;
            float radius;
            timeCounter += engine.getElapsedInLastFrame();
            if (timeCounter > 0.2f && timer < 0.5f) {
                timeCounter = 0f;
                radius = 1000f;
                Vector2f arcPos = new Vector2f(0f, 0f);
                VectorUtils.resize(directionNormalized(ship), radius, arcPos);
                if (ShaderLib.isOnScreen(ship.getLocation(), radius)) {
                    Vector2f t = new Vector2f(ship.getLocation().x + arcPos.x, ship.getLocation().y + arcPos.y);
                    Global.getSoundPlayer().playSound(SYSTEM_LIGHTNING, 0.2f, 0.25f, ship.getLocation(), t);
                    engine.spawnEmpArc(ship, ship.getLocation(), null, new SimpleEntity(t), DamageType.OTHER, 0f, 0f, 10000f, null, (float)Math.random() * (radius / 25f) + 5f, bluish, Color.WHITE);
                }
            }
            if (timer >= 0.1f) {
                Vector2f arcShift = new Vector2f(0f, 0f);
                VectorUtils.resize(directionNormalized(ship), 1000f, arcShift);
                if (timer < 0.5f) {
                    entryPoint = new Vector2f(ship.getLocation().x + arcShift.x, ship.getLocation().y + arcShift.y);
                }
                for(int i = 0; i < 1f * timer; ++i) {
                    radius = (float)arcRadius.get(ship.getHullSpec().getHullSize());
                    vel = MathUtils.getRandomPointInCircle(null, radius);
                    if (ShaderLib.isOnScreen(entryPoint, radius)) {
                        if ((float)Math.random() <= 0.5f) {
                            float duration = MathUtils.getRandomNumberInRange(0.25f, 1f);
                            float size = MathUtils.getRandomNumberInRange(5f, 20f);
                            engine.addSmoothParticle(entryPoint, vel, size, 1f, duration, bluish);
                        } else {
                            Vector2f t = new Vector2f(entryPoint.x + vel.x, entryPoint.y + vel.y);
                            engine.spawnEmpArc(ship, entryPoint, null, new SimpleEntity(t), DamageType.OTHER, 0f, 0f, 10000f, null, (float)Math.random() * (radius / 25f) + 5f, bluish, Color.WHITE);
                        }
                    }
                }
            }
        } else if (state == State.ACTIVE) { /********** State.ACTIVE **********/
            if(!engine.isPaused()){
                timer += engine.getElapsedInLastFrame();
            }

            /***** Move ship back a bit *****/
            if (!destSet) {
                originalDest = new Vector2f(ship.getLocation());
                VectorUtils.resize(VectorUtils.rotate(new Vector2f(1f, 0f), ship.getFacing() + 180), 500f, shiftedPosition);
                destination = new Vector2f(ship.getLocation().x + shiftedPosition.x, ship.getLocation().y + shiftedPosition.y);
                ship.getLocation().set(destination);
                destSet = true;
            }

            ship.getEngineController().fadeToOtherColor(this, Color.WHITE, null, 1f, 0.8f);
            ship.getEngineController().extendFlame(this, 5f, 0.25f, 1f);

            ship.getVelocity().set(new Vector2f(0f, 0f));
            stats.getMaxSpeed().modifyFlat(BUFF_SINGULARITY_GENERATOR, 1f);
            ship.setCollisionClass(CollisionClass.NONE);
            ship.setPhased(true);
            ship.setExtraAlphaMult(0f);
            ship.setApplyExtraAlphaToEngines(true);

            /***** Exit lightning *****/
            Vector2f vel;
            float radius;
            for(int i = 0; i < 1f * timer; ++i) {
                radius = (float)arcRadius.get(ship.getHullSpec().getHullSize());
                vel = MathUtils.getRandomPointInCircle(null, radius);
                if (ShaderLib.isOnScreen(ship.getLocation(), radius)) {
                    if ((float)Math.random() <= 0.5f) {
                        float duration = MathUtils.getRandomNumberInRange(0.25f, 1f);
                        float size = MathUtils.getRandomNumberInRange(5f, 20f);
                        engine.addSmoothParticle(ship.getLocation(), vel, size, 1f, duration, bluish);
                    } else {
                        Vector2f t = new Vector2f(ship.getLocation().x + vel.x, ship.getLocation().y + vel.y);
                        if (Math.abs(MathUtils.getShortestRotation(ship.getFacing(), VectorUtils.getAngle(ship.getLocation(), t))) > 20f) {
                            engine.spawnEmpArc(ship, ship.getLocation(), null, new SimpleEntity(t), DamageType.OTHER, 0f, 0f, 10000f, null, (float)Math.random() * (radius / 25f) + 5f, bluish, Color.WHITE);
                        }
                    }
                }
            }
            /***** Directional lightning *****/
            timeCounter += engine.getElapsedInLastFrame();
            if (timeCounter > 0.2f) {
                timeCounter = 0f;
                Global.getSoundPlayer().playSound(SYSTEM_LIGHTNING, 0.2f, 0.25f, ship.getLocation(), originalDest);
                radius = (float)arcRadius.get(ship.getHullSpec().getHullSize());
                if (ShaderLib.isOnScreen(ship.getLocation(), radius)) {
                    engine.spawnEmpArc(ship, ship.getLocation(), null, new SimpleEntity(originalDest), DamageType.OTHER, 0f, 0f, 10000f, null, (float)Math.random() * (radius / 25f) + 5f, bluish, Color.WHITE);
                }
            }
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (state == State.IN) {
            if (index == 0) {
                return new StatusData("Charging engines...", false);
            }
        } else if (state == State.ACTIVE) {
            if (index == 0) {
                return new StatusData("Generator Active", false);
            }
        }

        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        if (!(stats.getEntity() instanceof ShipAPI)) { return; }

        ShipAPI ship = (ShipAPI) stats.getEntity();
        CombatEngineAPI engine = Global.getCombatEngine();

        List<ShipEngineControllerAPI.ShipEngineAPI> engineList = ship.getEngineController().getShipEngines();
        Iterator i$ = engineList.iterator();
        while(i$.hasNext()) {
            ShipEngineControllerAPI.ShipEngineAPI shipEngine = (ShipEngineControllerAPI.ShipEngineAPI)i$.next();
//            engine.addSmokeParticle(shipEngine.getLocation(), new Vector2f(), 50f, 0.5f, 0.5f, Color.WHITE); // new Color(60, 50, 40)

            if (ship.getMutableStats().getMaxSpeed().getMultStatMod(DEBUFF_SIGNATURE_DAMPENER) != null) {
                shipEngine.disable();
            }
        }

        if (soundOn) {
            Global.getSoundPlayer().playSound(SYSTEM_EXIT, 1f, 1f, ship.getLocation(), new Vector2f(0f, 0f));
        }

        stats.getMaxSpeed().unmodify(BUFF_SINGULARITY_GENERATOR);
        stats.getMaxTurnRate().unmodify(BUFF_SINGULARITY_GENERATOR);
        stats.getTurnAcceleration().unmodify(BUFF_SINGULARITY_GENERATOR);
        stats.getAcceleration().unmodify(BUFF_SINGULARITY_GENERATOR);
        stats.getDeceleration().unmodify(BUFF_SINGULARITY_GENERATOR);
        ship.setCollisionClass(CollisionClass.SHIP);
        ship.setExtraAlphaMult(1f);
        ship.setPhased(false);
        Vector2f scaledVelocity = new Vector2f(0f, 0f);
        VectorUtils.resize(velocityVector, startSpeed, scaledVelocity);
        ship.getVelocity().set(scaledVelocity);

        timer = 0f;
        timeCounter = 0f;
        isActive = false;
        soundOn = false;
        destSet = false;
    }

    private Vector2f directionNormalized(ShipAPI ship) {
        return VectorUtils.rotate(new Vector2f(1f, 0f), ship.getFacing());
    }

    private float weightedAverage(float max, float start, float effectLevel) {
        return max * effectLevel + start * (1 - effectLevel);
    }
}