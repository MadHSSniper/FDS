package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static data.scripts.campaign.ids.FDS_IDs.DEBUFF_SIGNATURE_DAMPENER;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_QuantumGeneratorAIScript implements ShipSystemAIScript {

    private ShipAPI ship;
    private ShipwideAIFlags flags;
    private CombatEngineAPI engine;
    private ShipSystemAPI system;
    private final IntervalUtil tracker = new IntervalUtil(1f, 2f); // 0.1 - 0.2
    private final IntervalUtil evaluate = new IntervalUtil(2f, 4f);
    private CombatFleetManagerAPI.AssignmentInfo assignments;

    private static Map safeDistance = new HashMap();
    static {
        safeDistance.put(ShipAPI.HullSize.FIGHTER, 1f);
        safeDistance.put(ShipAPI.HullSize.FRIGATE, 1f);
        safeDistance.put(ShipAPI.HullSize.DESTROYER, 1f);
        safeDistance.put(ShipAPI.HullSize.CRUISER, 2f);
        safeDistance.put(ShipAPI.HullSize.CAPITAL_SHIP, 3f);
    }

    private enum ShipOrder {
        MOVE,
        ENGAGE,
        RETREAT,
        FLANK,
        CAPTURE,
        ESCORT,
    }
    private ShipOrder order;
    private ShipAPI target;

    public FDS_QuantumGeneratorAIScript() {}

    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;
    }

    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (this.engine != null) {
            if (this.engine.isPaused()) {
                return;
            }
            this.tracker.advance(amount);
            this.evaluate.advance(amount);
            if (this.evaluate.intervalElapsed()) {
                CombatFleetManagerAPI.AssignmentInfo assignments = engine.getFleetManager(ship.getOwner()).getTaskManager(false).getAssignmentFor(ship);

                if (assignments != null) {
                    if (this.assignments == null || assignments.getType() != this.assignments.getType()) {
                        this.assignments = assignments;
                    }
                } else {
                    if (this.flags.hasFlag(ShipwideAIFlags.AIFlags.PURSUING)
                      || this.flags.hasFlag(ShipwideAIFlags.AIFlags.HARASS_MOVE_IN) && !this.flags.hasFlag(ShipwideAIFlags.AIFlags.HARASS_MOVE_IN_COOLDOWN)) {
                        this.order = ShipOrder.ENGAGE;
                    } else if (this.flags.hasFlag(ShipwideAIFlags.AIFlags.BACK_OFF)
                      && this.flags.hasFlag(ShipwideAIFlags.AIFlags.DO_NOT_PURSUE)) {
                        this.order = ShipOrder.RETREAT;
                    }
                    this.order = null;
                }
            }

            if (this.tracker.intervalElapsed()) {

                engine.addSmoothParticle(this.ship.getMouseTarget(), new Vector2f(0f, 0f), 50f, 1f, 1f, Color.PINK);
                if (this.ship.getShipTarget() != null) {
                    engine.addSmoothParticle(this.ship.getShipTarget().getLocation(), new Vector2f(0f, 0f), 500f, 1f, 1f, Color.RED);
                }

                // System out of uses or on cooldown
                if (system.isOutOfAmmo() || (system.getCooldownRemaining() > 0 && !system.isOn())) {
                    return;
                }

                // Prevent activation when under the effect of the Gravity Well Generator
                if (ship.getMutableStats().getMaxSpeed().getMultStatMod(DEBUFF_SIGNATURE_DAMPENER) != null) {
                    if (this.system.isActive()) {
                        this.ship.useSystem();
                    }
                    return;
                }

                // Prevent activation when near max flux capacity
                if (ship.getFluxTracker().getFluxLevel() > 0.8f) {
                    return;
                }

                // Shut down if near the map's border
                if (this.system.isActive() && (this.flags.hasFlag(ShipwideAIFlags.AIFlags.AVOIDING_BORDER))) {
                    this.ship.useSystem();
                }

                // Prevent activation if no combat situation calls for the use of the system
                if (this.order == null) {
                    return;
                }

                switch (this.order) {
                    case MOVE:
                        executeMove();
                        break;
                    case ENGAGE:
                        executeEngage();
                        break;
                    case RETREAT:
                        executeRetreat();
                        break;
                    case FLANK:
                        executeFlank();
                        break;
                    case CAPTURE:
                        executeCapture();
                        break;
                    case ESCORT:
                        executeEscort(this.target);
                        break;
                }
            }
        }
    }

    private void executeMove() {
        // TODO
    }

    private void executeEngage() {
        // this.ship.getCaptain().getPersonalityAPI().getId()
        // TODO
    }

    private void executeRetreat() {
        // engine.getFleetManager(ship.getOwner()).getTaskManager(true).getAssignmentFor(ship);
        // TODO
    }

    private void executeFlank() {
        // TODO
    }

    private void executeCapture() {
        // TODO
    }

    private void executeEscort(ShipAPI escortee) {
        if (escortee == null) { return; }

        if (MathUtils.getDistance(this.ship.getLocation(), escortee.getLocation()) > 2000f) {
            Vector2f point = null;
            int tries = 0;

            while (point == null && tries < 10) {
                tries++;
                float distanceMult = (float)safeDistance.get(escortee.getHullSpec().getHullSize());
                Vector2f p = MathUtils.getRandomPointOnCircumference(escortee.getLocation(), 200f * distanceMult);

                List<ShipAPI> ships = CombatUtils.getShipsWithinRange(p, 200f);

                if (!ships.isEmpty()) {
                    Iterator i$ = ships.iterator();
                    ShipAPI ship;
                    while(i$.hasNext()) {
                        ship = (ShipAPI)i$.next();
                        if (ship.getHullSpec().getHullSize().equals(ShipAPI.HullSize.FIGHTER)) {
                            point = p;
                        }
                    }
                }
            }

            if (point != null) {
                this.ship.giveCommand(ShipCommand.USE_SYSTEM, point, 0);
            }
        }
    }

    private void closeIn(Vector2f location, boolean allied) {
        this.ship.giveCommand(ShipCommand.USE_SYSTEM, location, 0);
    }
}
