package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import org.dark.shaders.distortion.WaveDistortion;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.Iterator;
import java.util.List;

import static data.scripts.campaign.ids.FDS_IDs.BUFF_SINGULARITY_GENERATOR;
import static data.scripts.campaign.ids.FDS_IDs.DEBUFF_SIGNATURE_DAMPENER;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_YeeetDriveScript extends BaseShipSystemScript {

    private static final String EXIT_BOOM = "system_quantum_generator_exit";
    private static final Color RED = new Color(255, 50, 50,255);

    private boolean isActive = false;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        // instanceof also acts as a null check
        if (!(stats.getEntity() instanceof ShipAPI)) { return; }

        ShipAPI ship = (ShipAPI) stats.getEntity();
        CombatEngineAPI engine = Global.getCombatEngine();
        if (state == State.OUT) {
            if (isActive) {
//                engine.spawnExplosion(ship.getLocation(), new Vector2f(), BLUE, EXIT_BOOM_RADIUS, 0.5f);

                List<ShipEngineControllerAPI.ShipEngineAPI> engineList = ship.getEngineController().getShipEngines();
                Iterator i$ = engineList.iterator();
                while(i$.hasNext()) {
                    ShipEngineControllerAPI.ShipEngineAPI shipEngine = (ShipEngineControllerAPI.ShipEngineAPI)i$.next();
                    engine.addSmokeParticle(shipEngine.getLocation(), new Vector2f(), 50f, 0.5f, 0.5f, Color.RED); // new Color(60, 50, 40)
                }

                Global.getSoundPlayer().playSound(EXIT_BOOM, 1f, 1f, ship.getLocation(), ship.getVelocity());
                stats.getMaxSpeed().unmodify(BUFF_SINGULARITY_GENERATOR);
                isActive = false;
            }
//            wave = new WaveDistortion();
//            wave.setLocation(exit_boom_location);
//            wave.setSize(EXIT_BOOM_RADIUS * 2 * (1 - effectLevel));
//            wave.setIntensity(EXIT_BOOM_RADIUS * 0.1f);
//            wave.fadeInSize(1.2f);
//            wave.fadeOutIntensity(0.9f);
//            DistortionShader.addDistortion(wave);
        } else if (state == State.IN) {
            ship.getEngineController().fadeToOtherColor(this, RED, null, 1f, 0.8f);
            if (!isActive) {
                isActive = true;
//                if (engine.getPlayerShip() == ship) {
//                    if (ship.getHullLevel() > 0.3) {
//                        Global.getSoundPlayer().playSound(CHARGEUP_DEFAULT, 1f, 1f, ship.getLocation(), ship.getVelocity());
//                    } else {
//                        Global.getSoundPlayer().playSound(CHARGEUP_URGENT, 1f, 1f, ship.getLocation(), ship.getVelocity());
//                    }
//                }

//                Vector2f loc = ship.getLocation();
//                float direction = ship.getFacing();
//                Vector2f point;
//                float radius;
//                float angle;
//                for(int i = 0; i < 50; ++i) {
//                    point = MathUtils.getRandomPointInCircle(loc, 200);
//                    radius = MathUtils.getRandomNumberInRange(0f, 180f);
//                    angle = VectorUtils.getAngle(point, loc);
//
//                    Vector2f vel = MathUtils.getPointOnCircumference(null, radius, -direction);
//                    float duration = MathUtils.getRandomNumberInRange(0.5f, 2f);
//                    float size = MathUtils.getRandomNumberInRange(5f, 20f);
//                    Global.getCombatEngine().addSmoothParticle(point, vel, size, 1f, duration, Color.WHITE);
//                }
            } else {
                forceShutdown(engine, ship, state);
            }

        } else {
            boolean shutdown = forceShutdown(engine, ship, state);
            if (!shutdown) {
                if (ship.getMutableStats().getMaxSpeed().getMultStatMod(DEBUFF_SIGNATURE_DAMPENER) != null) {
                    unapply(ship.getMutableStats(), BUFF_SINGULARITY_GENERATOR);
                } else {
                    ship.getEngineController().fadeToOtherColor(this, Color.RED, null, 1f, 0.8f);
                    ship.getEngineController().extendFlame(this, 3f, 1f, 1f);

                    stats.getMaxSpeed().modifyFlat(BUFF_SINGULARITY_GENERATOR, 4000f * effectLevel);
                    stats.getAcceleration().modifyFlat(BUFF_SINGULARITY_GENERATOR, 20000f * effectLevel);
//                    ship.setCollisionClass(CollisionClass.NONE);
                }
            }
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (state == State.IN) {
            if (index == 0) {
                return new StatusData("Get ready to YEEEEET", true);
            }
        } else if (state == State.ACTIVE) {
            if (index == 0) {
                return new StatusData("YEEEEEEEEEEEEETTT!!!!", true);
            }
        }

        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        if (!(stats.getEntity() instanceof ShipAPI)) { return; }

        stats.getMaxSpeed().unmodify(BUFF_SINGULARITY_GENERATOR);
        stats.getMaxTurnRate().unmodify(BUFF_SINGULARITY_GENERATOR);
        stats.getTurnAcceleration().unmodify(BUFF_SINGULARITY_GENERATOR);
        stats.getAcceleration().unmodify(BUFF_SINGULARITY_GENERATOR);
        stats.getDeceleration().unmodify(BUFF_SINGULARITY_GENERATOR);

        ShipAPI ship = (ShipAPI) stats.getEntity();
        ship.setCollisionClass(CollisionClass.SHIP);
    }

    private boolean forceShutdown(CombatEngineAPI engine, ShipAPI ship, State state) {
        float height = engine.getMapHeight();
        float width = engine.getMapWidth();
        float x = ship.getLocation().x;
        float y = ship.getLocation().y;
        if (isActive && (state == State.IN || state == State.ACTIVE)) {
            if (x <= (-width/2) || x >= (width/2) || y <= (-height/2) || y >= (height/2)) {
                ship.useSystem();
                return true;
            }
            if (ship.getMutableStats().getMaxSpeed().getMultStatMod(DEBUFF_SIGNATURE_DAMPENER) != null) {
                ship.useSystem();
                return true;
            }
        }
        return false;
    }
}