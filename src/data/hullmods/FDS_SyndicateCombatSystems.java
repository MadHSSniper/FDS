package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.plugins.FDS_FactionCombatBuff;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
@SuppressWarnings("unchecked")
public class FDS_SyndicateCombatSystems extends BaseHullMod {

    private static final float TURN_BONUS = 20f; //20% increase
    private static final float RECOIL_BONUS = 25f; //25% increase
    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(1);
    private String ERROR = "FDSIncompatibleHullmodWarning";

    private static Map hulls = new HashMap();
    static {
        hulls.put(HullSize.FIGHTER, 0f);
        hulls.put(HullSize.FRIGATE, 5f);
        hulls.put(HullSize.DESTROYER, 10f);
        hulls.put(HullSize.CRUISER, 15f);
        hulls.put(HullSize.CAPITAL_SHIP, 20f);
    }

    static {
        BLOCKED_HULLMODS.add("turretgyros");
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                ship.getVariant().removeMod(tmp);
                ship.getVariant().addMod(ERROR);
            }
        }
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (!(ship.getHullSpec().getHullId().startsWith("fds_"))) {
            return "Must be installed on a FDS ship";
        }
        if (ship.getVariant().getHullMods().contains("turretgyros")) {
            return "Incompatible with Advanced Turret Gyros";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        // Allows any ship with a fds hull id
        return ship.getHullSpec().getHullId().startsWith("fds_") &&
            !ship.getVariant().getHullMods().contains("turretgyros");
    }

    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getEnergyWeaponRangeBonus().modifyPercent(id, (float) hulls.get(hullSize));
        stats.getWeaponTurnRateBonus().modifyPercent(id, TURN_BONUS);
        stats.getBeamWeaponTurnRateBonus().modifyPercent(id, TURN_BONUS);
        stats.getRecoilDecayMult().modifyPercent(id, RECOIL_BONUS);
    }

    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) TURN_BONUS;
        }
        if (index == 1) {
            return "" + (int) RECOIL_BONUS;
        }
        if (index == 2) {
            return "5/10/15/20";
        }
        if (index == 3) {
            return "" + (FDS_FactionCombatBuff.ALLIED_BOOST * 5);
        }
        if (index == 4) {
            return "" + (int) (FDS_FactionCombatBuff.MAX_BOOST * 100);
        }
        return null;
    }
}
