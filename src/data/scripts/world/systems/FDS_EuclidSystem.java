package data.scripts.world.systems;

import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin.DerelictShipData;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner.ShipRecoverySpecialCreator;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.PerShipData;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.ShipCondition;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.ids.FDS_Industries;
import data.scripts.world.FDS_AddMarket;
import data.scripts.campaign.ids.FDS_IDs;
import data.scripts.campaign.ids.Vanilla_PlanetTypes;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_EuclidSystem implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {

        StarSystemAPI system = sector.createStarSystem("Euclid");
        system.setBackgroundTextureFilename("graphics/backgrounds/background4.jpg");

        PlanetAPI star = system.initStar("euclidStar",
                StarTypes.BLUE_SUPERGIANT,
                1200f,
                900, // extent of corona outside star
                10f, // solar wind burn level
                1f, // flare probability
                5f); // CR loss multiplier, good values are in the range of 1-5
        system.setLightColor(new Color(240, 240, 255));
        star.setName("Euclid");

        system.getLocation().set(3000f, 7000f);

        SectorEntityToken euclidNebula = Misc.addNebulaFromPNG("data/campaign/terrain/fds_euclid_nebula.png",
                0, 0,
                system,
                "terrain", "nebula_blue",
                4, 4, StarAge.AVERAGE);

        PlanetAPI dolor = system.addPlanet("dolorPlanet", star, "Dolor", FDS_IDs.PLANET_LAVA, 300, 50, 3000, 100);
        Misc.initConditionMarket(dolor);
        dolor.getMarket().addCondition(Conditions.EXTREME_TECTONIC_ACTIVITY);
        dolor.getMarket().addCondition(Conditions.VERY_HOT);
        dolor.getMarket().addCondition(Conditions.METEOR_IMPACTS);
        dolor.getMarket().addCondition(Conditions.ORE_ABUNDANT);
        dolor.getMarket().addCondition(Conditions.VOLATILES_DIFFUSE);

        PlanetAPI angor = system.addPlanet("angorPlanet", star, "Angor", Vanilla_PlanetTypes.TOXIC, 240, 30, 4000, 150);
        Misc.initConditionMarket(angor);
        angor.getMarket().addCondition(Conditions.HOT);
        angor.getMarket().addCondition(Conditions.IRRADIATED);
        angor.getMarket().addCondition(Conditions.TOXIC_ATMOSPHERE);
        angor.getMarket().addCondition(Conditions.INIMICAL_BIOSPHERE);
        angor.getMarket().addCondition(Conditions.DENSE_ATMOSPHERE);
        angor.getMarket().addCondition(Conditions.EXTREME_WEATHER);
        angor.getMarket().addCondition(Conditions.VOLATILES_PLENTIFUL);
        angor.getMarket().addCondition(Conditions.ORGANICS_PLENTIFUL);

        PlanetAPI rabidus = system.addPlanet("rabidusPlanet", star, "Rabidus", Vanilla_PlanetTypes.ROCKY_METALLIC, 120, 40, 7000, 200);
        Misc.initConditionMarket(rabidus);
        rabidus.getMarket().addCondition(Conditions.HOT);
        rabidus.getMarket().addCondition(Conditions.VOLATILES_TRACE);
        rabidus.getMarket().addCondition(Conditions.ORE_ULTRARICH);
        rabidus.getMarket().addCondition(Conditions.RARE_ORE_ABUNDANT);
        rabidus.getMarket().addCondition(Conditions.NO_ATMOSPHERE);

        PlanetAPI insanus = system.addPlanet("insanusPlanet", star, "Insanus", FDS_IDs.PLANET_GAS_GIANT, 180, 320, 9000, 300);
        system.addRingBand(insanus, "misc", "rings_ice0", 256f, 1, Color.white, 256f, 700, 45, Terrain.RING, null);

        PlanetAPI insanus1 = system.addPlanet("insanus1Planet", insanus, "Insanus 1", Vanilla_PlanetTypes.VOLCANIC_MINOR, 120, 30, 450, 30);
        Misc.initConditionMarket(insanus1);
        insanus1.getMarket().addCondition(Conditions.VERY_HOT);
        insanus1.getMarket().addCondition(Conditions.TECTONIC_ACTIVITY);
        insanus1.getMarket().addCondition(Conditions.ORE_RICH);
        insanus1.getMarket().addCondition(Conditions.RARE_ORE_SPARSE);
        insanus1.getMarket().addCondition(Conditions.DENSE_ATMOSPHERE);

        PlanetAPI insanus2 = system.addPlanet("insanus2Planet", insanus, "Insanus 2", Vanilla_PlanetTypes.BARREN_BOMBARDED, 10, 40, 600, 40);
        Misc.initConditionMarket(insanus2);
        insanus2.getMarket().addCondition(Conditions.RUINS_SCATTERED);
        insanus2.getMarket().addCondition(Conditions.METEOR_IMPACTS);
        insanus2.getMarket().addCondition(Conditions.RARE_ORE_RICH);
        insanus2.getMarket().addCondition(Conditions.RARE_ORE_MODERATE);
        insanus2.getMarket().addCondition(Conditions.NO_ATMOSPHERE);

        PlanetAPI insanus3 = system.addPlanet("insanus3Planet", insanus, "Insanus 3", Vanilla_PlanetTypes.TUNDRA, 90, 38, 1000, 70);
        Misc.initConditionMarket(insanus3);
        insanus3.getMarket().addCondition(Conditions.RUINS_VAST);
        insanus3.getMarket().addCondition(Conditions.COLD);
        insanus3.getMarket().addCondition(Conditions.VOLATILES_ABUNDANT);
        insanus3.getMarket().addCondition(Conditions.FARMLAND_POOR);
        insanus3.getMarket().addCondition(Conditions.HABITABLE);

        PlanetAPI delirus = system.addPlanet("delirusPlanet", star, "Delirus", Vanilla_PlanetTypes.ICE_GIANT, 0, 250, 12000, 400);
        system.addRingBand(delirus, "misc", "rings_ice0", 256f, 3, Color.white, 300f, 800, 45, Terrain.RING, null);

        PlanetAPI vindicta = system.addPlanet("vindictaPlanet", delirus, "Vindicta", FDS_IDs.PLANET_TUNDRA, 25, 72, 1200, 60);
        system.addAsteroidBelt(vindicta, 20, 600, 100, 500, 600);
        system.addRingBand(vindicta, "misc", "rings_asteroids0", 256f, 0, Color.white, 120f, 600, 550f);
        vindicta.setFaction(FDS_IDs.FACTION_FDS);
        FDS_AddMarket.FDS_AddMarket(FDS_IDs.FACTION_FDS,
                vindicta,
                null,
                "Vindicta",
                5,
                new ArrayList<>(Arrays.asList(
                        Conditions.FARMLAND_POOR,
                        Conditions.HABITABLE,
//                        Conditions.SPACEPORT,
                        Conditions.POPULATION_3,
                        Conditions.COLD,
                        Conditions.ORE_MODERATE,
                        Conditions.POOR_LIGHT,
                        Conditions.OUTPOST)),
                new ArrayList<>(Arrays.asList(
                        Industries.LIGHTINDUSTRY,
                        Industries.HEAVYINDUSTRY,
                        Industries.SPACEPORT,
                        Industries.FARMING,
                        Industries.MINING,
                        Industries.PATROLHQ,
                        Industries.POPULATION,
                        FDS_Industries.REPAIR_YARDS)),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY)),
                0.3f
        );

//        JumpPointAPI vindictaJumpPoint = Global.getFactory().createJumpPoint("vindictaJumpPoint", "Vindicta Jump-Point");
//        OrbitAPI orbit = Global.getFactory().createCircularOrbit(delirus, 15, 2000, 60);
//        vindictaJumpPoint.setOrbit(orbit);
//        vindictaJumpPoint.setRelatedPlanet(vindicta);
//        vindictaJumpPoint.setStandardWormholeToHyperspaceVisual();
//        system.addEntity(vindictaJumpPoint);

        system.autogenerateHyperspaceJumpPoints(true, true, true);

        cleanup(system);
    }

    void cleanup(StarSystemAPI system) {
        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);
        float minRadius = plugin.getTileSize() * 2f;

        float radius = system.getMaxRadiusInHyperspace();
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius * 0.5f, 0, 360f);
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius, 0, 360f, 0.25f);
    }

    protected void addDerelict(StarSystemAPI system, SectorEntityToken focus, String variantId,
                               ShipCondition condition, float orbitRadius, boolean recoverable) {
        DerelictShipData params = new DerelictShipData(new PerShipData(variantId, condition), false);
        SectorEntityToken ship = BaseThemeGenerator.addSalvageEntity(system, Entities.WRECK, Factions.NEUTRAL, params);
        ship.setDiscoverable(true);

        float orbitDays = orbitRadius / (10f + (float) Math.random() * 5f);
        ship.setCircularOrbit(focus, (float) Math.random() * 360f, orbitRadius, orbitDays);

        if (recoverable) {
            ShipRecoverySpecialCreator creator = new ShipRecoverySpecialCreator(null, 0, 0, false, null, null);
            Misc.setSalvageSpecial(ship, creator.createSpecial(ship, null));
        }

    }
}
