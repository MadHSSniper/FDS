package data.scripts.world;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import data.scripts.campaign.ids.FDS_IDs;
import data.scripts.world.systems.*;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
@SuppressWarnings("unchecked")
public class FDSGen implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {
        //sector generation code
        new FDS_ArchimedesSystem().generate(sector);
        new FDS_EuclidSystem().generate(sector);
        new FDS_HypatiaSystem().generate(sector);
        new FDS_PythagorasSystem().generate(sector);

        LocationAPI hyper = Global.getSector().getHyperspace();
        SectorEntityToken fringeLabel = hyper.addCustomEntity(FDS_IDs.LABEL_FRINGE, "The Fringe Worlds", FDS_IDs.LABEL_FRINGE, null);
        fringeLabel.setFixedLocation(8000f, 20000f);

        SharedData.getData().getPersonBountyEventData().addParticipatingFaction(FDS_IDs.FACTION_FDS);
        
        setVanillaRelationships(sector);
    }

    private void setVanillaRelationships(SectorAPI sector) {

        FactionAPI syndicate = sector.getFaction(FDS_IDs.FACTION_FDS);
        FactionAPI player = sector.getFaction(Factions.PLAYER);
        FactionAPI hegemony = sector.getFaction(Factions.HEGEMONY);
        FactionAPI tritachyon = sector.getFaction(Factions.TRITACHYON);
        FactionAPI pirates = sector.getFaction(Factions.PIRATES);
        FactionAPI independent = sector.getFaction(Factions.INDEPENDENT);
        FactionAPI church = sector.getFaction(Factions.LUDDIC_CHURCH);
        FactionAPI path = sector.getFaction(Factions.LUDDIC_PATH);
        FactionAPI kol = sector.getFaction(Factions.KOL);
        FactionAPI diktat = sector.getFaction(Factions.DIKTAT);
        FactionAPI persean = sector.getFaction(Factions.PERSEAN);
        FactionAPI lion = sector.getFaction(Factions.LIONS_GUARD);
        FactionAPI remnant = sector.getFaction(Factions.REMNANTS);

        syndicate.setRelationship(player.getId(), RepLevel.NEUTRAL);           // -0.05f
        syndicate.setRelationship(hegemony.getId(), RepLevel.VENGEFUL);        // -0.5f
        syndicate.setRelationship(tritachyon.getId(), RepLevel.FAVORABLE);     // 0.15f
        syndicate.setRelationship(pirates.getId(), RepLevel.VENGEFUL);         // -0.75f
        syndicate.setRelationship(independent.getId(), RepLevel.FRIENDLY);     // -0.2f
        syndicate.setRelationship(church.getId(), RepLevel.INHOSPITABLE);      // -0.4f
        syndicate.setRelationship(path.getId(), RepLevel.VENGEFUL);            // -0.7f
        syndicate.setRelationship(kol.getId(), RepLevel.INHOSPITABLE);         // -0.4f
        syndicate.setRelationship(diktat.getId(), RepLevel.INHOSPITABLE);      // -0.45f
        syndicate.setRelationship(persean.getId(), RepLevel.INHOSPITABLE);     // -0.4f
        syndicate.setRelationship(lion.getId(), RepLevel.INHOSPITABLE);        // -0.4f
        syndicate.setRelationship(remnant.getId(), RepLevel.HOSTILE);

        setModRelationships(syndicate);
    }

    private void setModRelationships(FactionAPI faction) {
        //same generic relationship between most modded factions
        faction.setRelationship("approlight", -0.8f);
        faction.setRelationship("blackrock_driveyards", 0.05f);
        faction.setRelationship("cabal", -0.6f);
        faction.setRelationship("citadeldefenders", 0.1f);
        faction.setRelationship("corvus_scavengers", 0f);
        faction.setRelationship("darkspire", -0.1f);
        faction.setRelationship("dassault_mikoyan", 0.15f);
        faction.setRelationship("diableavionics", 0.01f);
        faction.setRelationship("exigency", -0.2f);
        faction.setRelationship("exipirated", 0f);
        faction.setRelationship("famous_bounty", 0f);
        faction.setRelationship("immortallight", 0f);
        faction.setRelationship("interstellarimperium", -0.4f);
        faction.setRelationship("junk_pirates", -0.2f);
        faction.setRelationship("Lte", 0f);
        faction.setRelationship("mayorate", -0.3f);
        faction.setRelationship("metelson", 0.3f);
        faction.setRelationship("neutrinocorp", -0.1f);
        faction.setRelationship("noir", 0f);
        faction.setRelationship("nomads", 0f);
        faction.setRelationship("ORA", -0.25f);
        faction.setRelationship("pack", 0f);
        faction.setRelationship("pbc", 0f);
        faction.setRelationship("scavengers", 0f);
        faction.setRelationship("SCY", 0.1f);
        faction.setRelationship("spire", 0f);
        faction.setRelationship("shadow_industry", -0.7f);
        faction.setRelationship("syndicate_asp", -0.2f);
        faction.setRelationship("sun_ice", 0f);
        faction.setRelationship("sun_ici", 0f);
        faction.setRelationship("templars", -0.4f);
        faction.setRelationship("the_deserter", 0f);
        faction.setRelationship("tiandong", 0.2f);
    }
}
