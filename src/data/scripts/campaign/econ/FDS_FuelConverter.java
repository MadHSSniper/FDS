package data.scripts.campaign.econ;

import com.fs.starfarer.api.impl.campaign.econ.impl.BaseIndustry;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.util.Pair;
import data.scripts.campaign.ids.FDS_Commodities;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_FuelConverter extends BaseIndustry {

	public void apply() {
		super.apply(true);

		int size = market.getSize();

		demand(FDS_Commodities.ENERGY_CRYSTALS, size - 1);

		supply(Commodities.FUEL, size - 1);

		Pair<String, Integer> deficit = getMaxDeficit(FDS_Commodities.ENERGY_CRYSTALS);

		applyDeficitToProduction(1, deficit, Commodities.FUEL);

		if (!isFunctional()) {
			supply.clear();
		}
	}

	@Override
	public void unapply() {
		super.unapply();
	}

	@Override
	public boolean isAvailableToBuild() {
		if (!super.isAvailableToBuild()) return false;
		if (market.hasIndustry(Industries.FUELPROD)) {
			return false;
		}
		return true;
	}

	@Override
	public String getUnavailableReason() {
		if (!super.isAvailableToBuild()) return super.getUnavailableReason();
		return "Mutually exclusive with the Fuel Production industry";
	}
}
