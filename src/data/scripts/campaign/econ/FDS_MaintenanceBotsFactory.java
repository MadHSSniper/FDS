package data.scripts.campaign.econ;

import com.fs.starfarer.api.impl.campaign.econ.impl.BaseIndustry;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.util.Pair;
import data.scripts.campaign.ids.FDS_Commodities;
import data.scripts.campaign.ids.FDS_Industries;


/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_MaintenanceBotsFactory extends BaseIndustry {

	public void apply() {
		super.apply(true);

		int size = market.getSize();

		demand(Commodities.HEAVY_MACHINERY, size - 1);
		demand(Commodities.VOLATILES, size - 2);
		demand(Commodities.METALS, size + 1);
		demand(Commodities.RARE_METALS, size - 2);

		supply(FDS_Commodities.MAINTENANCE_BOTS, size - 3);

		Pair<String, Integer> deficit = getMaxDeficit(Commodities.HEAVY_MACHINERY, Commodities.METALS, Commodities.RARE_METALS);
		applyDeficitToProduction(1, deficit, FDS_Commodities.MAINTENANCE_BOTS);

		if (!isFunctional()) {
			supply.clear();
		}
	}


	@Override
	public void unapply() {
		super.unapply();
	}

}
