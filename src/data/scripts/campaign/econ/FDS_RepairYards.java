package data.scripts.campaign.econ;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.econ.CommoditySpecAPI;
import com.fs.starfarer.api.impl.campaign.econ.impl.BaseIndustry;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Pair;
import data.scripts.campaign.ids.FDS_Commodities;

import java.awt.*;


/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_RepairYards extends BaseIndustry {

	public static float QUALITY_BONUS = 0.25f; // 25% increase

	public void apply() {
		super.apply(true);

		int size = market.getSize();

		demand(FDS_Commodities.MAINTENANCE_BOTS, size - 1);
		demand(Commodities.SUPPLIES, size - 2);
		demand(Commodities.HEAVY_MACHINERY, size - 2);

		Pair<String, Integer> deficit = getMaxDeficit(FDS_Commodities.MAINTENANCE_BOTS, Commodities.HEAVY_MACHINERY, Commodities.SUPPLIES);
		float bonus = QUALITY_BONUS;

		if (deficit.two > 0) {
			float total = 3 * size - 5;
			bonus = bonus * (1 - (deficit.two / total));
		}

		market.getStats().getDynamic().getMod(Stats.PRODUCTION_QUALITY_MOD).modifyMult(getModId(), 1f + bonus, getNameForModifier());
		market.getStats().getDynamic().getMod(Stats.FLEET_QUALITY_MOD).modifyMult(getModId(), 1f + bonus, getNameForModifier());

		if (!isFunctional()) {
			supply.clear();
		}
	}

	@Override
	public void unapply() {
		super.unapply();

		market.getStats().getDynamic().getMod(Stats.PRODUCTION_QUALITY_MOD).unmodifyMult(getModId());
		market.getStats().getDynamic().getMod(Stats.FLEET_QUALITY_MOD).unmodifyMult(getModId());
	}

	@Override
	protected void applyAlphaCoreModifiers() {
	}

	@Override
	protected void applyNoAICoreModifiers() {
	}

	@Override
	protected void applyAlphaCoreSupplyAndDemandModifiers() {
		demandReduction.modifyFlat(getModId(0), DEMAND_REDUCTION, "Alpha core");
	}

	protected void addAlphaCoreDescription(TooltipMakerAPI tooltip, AICoreDescriptionMode mode) {
		float opad = 10f;
		Color highlight = Misc.getHighlightColor();

		String pre = "Alpha-level AI core currently assigned. ";
		if (mode == AICoreDescriptionMode.MANAGE_CORE_DIALOG_LIST || mode == AICoreDescriptionMode.INDUSTRY_TOOLTIP) {
			pre = "Alpha-level AI core. ";
		}
		if (mode == AICoreDescriptionMode.INDUSTRY_TOOLTIP) {
			CommoditySpecAPI coreSpec = Global.getSettings().getCommoditySpec(aiCoreId);
			TooltipMakerAPI text = tooltip.beginImageWithText(coreSpec.getIconName(), 48);
			text.addPara(pre + "Reduces upkeep cost by %s. Reduces demand by %s unit.", opad, highlight,
					"" + (int)((1f - UPKEEP_MULT) * 100f) + "%", "" + DEMAND_REDUCTION);
			tooltip.addImageWithText(opad);
			return;
		}

		tooltip.addPara(pre + "Reduces upkeep cost by %s. Reduces demand by %s unit.", opad, highlight,
				"" + (int)((1f - UPKEEP_MULT) * 100f) + "%", "" + DEMAND_REDUCTION);
	}
	public boolean isAvailableToBuild() {
		return market.hasIndustry("heavyindustry") || market.hasIndustry("orbitalworks");
	}

	public String getUnavailableReason() {
		return "Requires Heavy Industry or Orbital Works";
	}
}
