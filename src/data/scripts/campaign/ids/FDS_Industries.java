package data.scripts.campaign.ids;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_Industries {
    public static final String MAINTENANCE_BOTS_FACTORY = "fds_maintenance_bots_factory";
    public static final String REPAIR_YARDS = "fds_repair_yards";
    public static final String FUEL_CONVERTER = "fds_fuel_converter";
}
