package data.scripts.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BuffManagerAPI.Buff;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.FDSPlugin;
import data.scripts.campaign.ids.FDS_Commodities;
import data.scripts.campaign.ids.FDS_IDs;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapted from an older version of NEXERELIN
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_MaintenanceBotsBonus implements EveryFrameScript {

    private static final float INTERVAL = 0.5f;
    private static final float CR_PERCENT = 50f; // 50%
    private static final float SUPPLY_PERCENT = -20f; // 20%

    private final List<CampaignFleetAPI> fleets = new ArrayList<>(500);
    private final IntervalUtil interval = new IntervalUtil(INTERVAL, INTERVAL);

    @Override
    public void advance(float amount) {
        if (!FDSPlugin.droidMechanics) {
            return;
        }

        interval.advance(amount);
        if (interval.intervalElapsed()) {
            fleets.clear();
            fleets.addAll(Global.getSector().getHyperspace().getFleets());
            List<StarSystemAPI> systems = Global.getSector().getStarSystems();
            for (StarSystemAPI system : systems) {
                fleets.addAll(system.getFleets());
            }

            for (CampaignFleetAPI fleet : fleets) {
                if (fleet.isAIMode() || !fleet.isAlive()) {
                    continue;
                }

                float bots = fleet.getCargo().getCommodityQuantity(FDS_Commodities.MAINTENANCE_BOTS);
                float suppliesPerMonth = 0f;
                for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                    if (!member.isMothballed()) {
                        suppliesPerMonth += member.getStats().getSuppliesPerMonth().getModifiedValue();
                    }
                }

                float totalRecoveryBonus = (bots / 2) / suppliesPerMonth;
                float recoveryPct = 0f;
                float supplyPct = 0f;

                if (totalRecoveryBonus > 1) {
                    recoveryPct = CR_PERCENT;
                    supplyPct = SUPPLY_PERCENT;
                } else if (totalRecoveryBonus < 0) {
                    recoveryPct = 0f;
                    supplyPct = 0f;
                } else {
                    recoveryPct = totalRecoveryBonus * CR_PERCENT;
                    supplyPct = totalRecoveryBonus * SUPPLY_PERCENT;
                }

                for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                    Buff buff = member.getBuffManager().getBuff(FDS_IDs.BUFF_MAINTENANCE_BOTS);
                    if (buff instanceof FDS_MaintenanceBotsBuff) {
                        if (recoveryPct > 0f) {
                            FDS_MaintenanceBotsBuff crewBonusBuff = (FDS_MaintenanceBotsBuff) buff;
                            crewBonusBuff.setBuffAmount(recoveryPct, supplyPct, member);
                        } else {
                            member.getBuffManager().removeBuff(FDS_IDs.BUFF_MAINTENANCE_BOTS);
                        }
                    } else if (recoveryPct > 0f) {
                        member.getBuffManager().addBuff(new FDS_MaintenanceBotsBuff(recoveryPct, supplyPct));
                    }
                }
            }
        }
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return true;
    }

    private static class FDS_MaintenanceBotsBuff implements Buff {

        private float crBuffAmount;
        private float supplyBuffAmount;
        private boolean expired = false;
        private final IntervalUtil interval = new IntervalUtil(1f, 1f);
        private transient FleetMemberAPI lastMember = null;

        FDS_MaintenanceBotsBuff(float crBuffAmount, float supplyBuffAmount) {
            this.crBuffAmount = crBuffAmount;
            this.supplyBuffAmount = supplyBuffAmount;
        }

        @Override
        public void advance(float days) {
            interval.advance(days);
            if (interval.intervalElapsed()) {
                if (lastMember != null) {
                    if (lastMember.getFleetData() == null
                    || lastMember.getFleetData().getFleet() == null
                    || lastMember.getFleetData().getFleet().isAIMode()
                    || !lastMember.getFleetData().getFleet().isAlive()) {
                        expired = true;
                    }
                }
            }
        }

        @Override
        public void apply(FleetMemberAPI member) {
            lastMember = member;
            member.getStats().getBaseCRRecoveryRatePercentPerDay().modifyPercent(FDS_IDs.BUFF_MAINTENANCE_BOTS, crBuffAmount);
            member.getStats().getRepairRatePercentPerDay().modifyPercent(FDS_IDs.BUFF_MAINTENANCE_BOTS, crBuffAmount);

            member.getStats().getSuppliesPerMonth().modifyPercent(FDS_IDs.BUFF_MAINTENANCE_BOTS, supplyBuffAmount);
//            member.getStats().getSuppliesToRecover().modifyPercent(FDS_IDs.BUFF_MAINTENANCE_BOTS, supplyBuffAmount);
        }

        @Override
        public String getId() {
            return FDS_IDs.BUFF_MAINTENANCE_BOTS;
        }

        @Override
        public boolean isExpired() {
            return expired;
        }

        void setBuffAmount(float crBuffAmount, float supplyBuffAmount, FleetMemberAPI member) {
            if (crBuffAmount != this.crBuffAmount || supplyBuffAmount != this.supplyBuffAmount) {
                apply(member);
            }
            this.crBuffAmount = crBuffAmount;
            this.supplyBuffAmount = supplyBuffAmount;
        }
    }
}
