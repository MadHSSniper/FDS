package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import java.awt.Color;
import java.util.*;

import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_ONE;

/**
 *
 * @author Dark.Revenant, SWP_SplitterWeaponPlugin for the Gungnir weapon, edited by MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_ProjectilePlugin extends BaseEveryFrameCombatPlugin {

    private static final String DATA_KEY = "FDS_ProjectileData";

    private static final float KANE_DEFAULT_RANGE = 1000f;
    private static final float KANE_DEFAULT_SPEED = 800f;
    private static final Color KANE_DETONATION_COLOR = new Color(255, 50, 50, 225);
    private static final float KANE_DETONATION_DURATION = 0.2f;
    private static final float KANE_DETONATION_SIZE = 20f;
    private static final String KANE_DETONATION_SOUND_ID = "fds_heavy_assault_gun";
    private static final float KANE_FUSE_DISTANCE = 100f;
    private static final Color KANE_PARTICLE_COLOR = new Color(255, 100, 100, 200);
    private static final int KANE_PARTICLE_COUNT = 20;
    private static final String KANE_PROJECTILE_ID = "fds_kane_shot";
    private static final float KANE_SPLIT_DISTANCE = 500f;
    private static final float KANE_SPREAD_FORCE_MAX = 50f;
    private static final float KANE_SPREAD_FORCE_MIN = 25f;
    private static final int KANE_SUBMUNITIONS = 10;
    private static final String KANE_SUBMUNITION_WEAPON_ID = "fds_kane_sub";

    private static final float ORIGINAL_PROJECTILE_DAMAGE_MULTIPLIER = 0.5f;


    private static final String MINE_PROJECTILE_ID = "fds_stasis_mine";
    private static final Color MINE_DETONATION_COLOR = new Color(53, 81, 255, 225);
    private static final float MINE_DETONATION_DURATION = 0.2f;
    private static final float MINE_DETONATION_SIZE = 20f;
    private static final String MINE_DETONATION_SOUND = "mine_explosion";


    private Map<DamagingProjectileAPI, Float> projectileTrailIDs = new HashMap();
    private Map<DamagingProjectileAPI, Float> projectileSecondaryTrailIDs = new HashMap();

    private Set<DamagingProjectileAPI> kaneProjectileSet;
    private Set<DamagingProjectileAPI> mineProjectileSet;
    private Set<DamagingProjectileAPI> trailingProjectileSet;
    private CombatEngineAPI engine;

    private static final Map<String, String> TRAIL_SPRITES = new HashMap<String, String>();
    static {
        TRAIL_SPRITES.put("fds_dual_autoblaster_cannon_shot", "fds_jagged_trail");
        TRAIL_SPRITES.put("fds_decimator_shot", "fds_trail");
    }

    private static final Map<String, Float> DURATION_IN = new HashMap<String, Float>();
    static {
        DURATION_IN.put("fds_dual_autoblaster_cannon_shot", 0f);
        DURATION_IN.put("fds_decimator_shot", 0f);
    }

    private static final Map<String, Float> DURATION_MAIN = new HashMap<String, Float>();
    static {
        DURATION_MAIN.put("fds_dual_autoblaster_cannon_shot", 0.1f);
        DURATION_MAIN.put("fds_decimator_shot", 0f);
    }

    private static final Map<String, Float> DURATION_FADE = new HashMap<String, Float>();
    static {
        DURATION_FADE.put("fds_dual_autoblaster_cannon_shot", 0.1f);
        DURATION_FADE.put("fds_decimator_shot", 0.75f);
    }

    private static final Map<String, Float> START_SIZE = new HashMap<String, Float>();
    static {
        START_SIZE.put("fds_dual_autoblaster_cannon_shot", 25f);
        START_SIZE.put("fds_decimator_shot", 15f);
    }

    private static final Map<String, Float> END_SIZE = new HashMap<String, Float>();
    static {
        END_SIZE.put("fds_dual_autoblaster_cannon_shot", 20f);
        END_SIZE.put("fds_decimator_shot", 30f);
    }

    private static final Map<String, Color> TRAIL_COLOR = new HashMap<String, Color>();
    static {
        TRAIL_COLOR.put("fds_dual_autoblaster_cannon_shot", new Color(0, 200, 0));
        TRAIL_COLOR.put("fds_decimator_shot", new Color(250,50,50));
    }

    private static final Map<String, Color> FADE_COLOR = new HashMap<String, Color>();
    static {
        FADE_COLOR.put("fds_dual_autoblaster_cannon_shot", new Color(120, 120, 120));
        FADE_COLOR.put("fds_decimator_shot", new Color(75,75,75));
    }

    private static final Map<String, Float> TRAIL_OPACITY = new HashMap<String, Float>();
    static {
        TRAIL_OPACITY.put("fds_dual_autoblaster_cannon_shot", 0.75f);
        TRAIL_OPACITY.put("fds_decimator_shot", 0.8f);
    }

    private static final Map<String, Float> LOOP_LENGTH = new HashMap<String, Float>();
    static {
        LOOP_LENGTH.put("fds_dual_autoblaster_cannon_shot", -1f);
        LOOP_LENGTH.put("fds_decimator_shot", -1f);
    }

    private static final Map<String, Float> SCROLL_SPEED = new HashMap<String, Float>();
    static {
        SCROLL_SPEED.put("fds_dual_autoblaster_cannon_shot", 0f);
        SCROLL_SPEED.put("fds_decimator_shot", 0f);
    }

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        if (engine == null || engine.isPaused()) {
            return;
        }

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        kaneProjectileSet = localData.kaneProjectileSet;
        mineProjectileSet = localData.mineProjectileSet;
        trailingProjectileSet = localData.trailingProjectileSet;

        List<DamagingProjectileAPI> projectiles = engine.getProjectiles();
        int size = projectiles.size();
        for (int i = 0; i < size; i++) {
            DamagingProjectileAPI proj = projectiles.get(i);
            String spec = proj.getProjectileSpecId();
            if (spec == null) {
                continue;
            }

            this.processProjectileTrail(proj);
            this.processProjectileFlicker(proj);

            switch (spec) {
                case KANE_PROJECTILE_ID:
                    this.processKane(proj);
                    break;
                case MINE_PROJECTILE_ID:
                    this.processMine(proj);
                    break;
                default:
                    continue;
            }
        }
    }

    private void processKane(DamagingProjectileAPI proj) {
        String newSpec;
        int submunitions;
        float fuseDistance;
        float splitDistance;
        float splitForceMin;
        float splitForceMax;
        float defaultRange;
        float defaultSpeed;
        Color detonateColor;
        float detonateSize;
        float detonateDuration;
        String detonateSound;
        Color particleColor;
        int particleCount;

        newSpec = KANE_SUBMUNITION_WEAPON_ID;
        submunitions = KANE_SUBMUNITIONS;
        fuseDistance = KANE_FUSE_DISTANCE;
        splitDistance = KANE_SPLIT_DISTANCE;
        splitForceMin = KANE_SPREAD_FORCE_MAX;
        splitForceMax = KANE_SPREAD_FORCE_MIN;
        defaultRange = KANE_DEFAULT_RANGE;
        defaultSpeed = KANE_DEFAULT_SPEED;
        detonateColor = KANE_DETONATION_COLOR;
        detonateSize = KANE_DETONATION_SIZE;
        detonateDuration = KANE_DETONATION_DURATION;
        detonateSound = KANE_DETONATION_SOUND_ID;
        particleColor = KANE_PARTICLE_COLOR;
        particleCount = KANE_PARTICLE_COUNT;

        if (proj.isFading() || proj.didDamage()) {
            // There is a potential memory leak if a script removes these projectiles directly
            // However, this is unlikely, and the set get flushed with each combat
            // As a result, we can safely ignore this potential problem (it would be fairly CPU-intensive to solve)
            kaneProjectileSet.remove(proj);
            return;
        }

        if (!kaneProjectileSet.contains(proj)) {
            kaneProjectileSet.add(proj);
            proj.getDamage().setDamage(proj.getDamage().getDamage() * ORIGINAL_PROJECTILE_DAMAGE_MULTIPLIER);
        }

        boolean shouldSplit = false;
        Vector2f loc = proj.getLocation();
        Vector2f vel = proj.getVelocity();
        float speedScalar;
        float rangeScalar;
        if (proj.getSource() != null) {
            speedScalar = proj.getSource().getMutableStats().getProjectileSpeedMult().getModifiedValue();
            rangeScalar = proj.getSource().getMutableStats().getBallisticWeaponRangeBonus().computeEffective(
                    defaultRange) / defaultRange;
        } else {
            rangeScalar = 1f;
            speedScalar = 1f;
        }
        float speed = defaultSpeed * speedScalar;

        // Real quick and dirty fuse distance that works in most cases
        float fuseTime = fuseDistance / speed;
        if (proj.getElapsed() < fuseTime) {
            return;
        }

        splitDistance *= rangeScalar;

        // This is some bullshit to make the weapon fade sooner than normal
        float detonateTime;
        if (proj.getWeapon() != null) {
            detonateTime = (proj.getWeapon().getRange() - splitDistance) / speed;
        } else {
            detonateTime = (defaultRange - splitDistance) / speed;
        }
        if (proj.getElapsed() >= detonateTime) {
            shouldSplit = true;
        }

        if (!shouldSplit) {
            // Check to see if the projectile should detonate
            Vector2f projection = new Vector2f(splitDistance, 0f);
            VectorUtils.rotate(projection, proj.getFacing(), projection);
            Vector2f.add(loc, projection, projection);

            List<ShipAPI> checkList = engine.getShips();
            List<ShipAPI> finalList = new LinkedList<>();
            int listSize = checkList.size();
            for (int j = 0; j < listSize; j++) {
                ShipAPI ship = checkList.get(j);
                boolean isInShields = false;
                if (ship.getShield() != null && ship.getShield().isOn()) {
                    if (MathUtils.isWithinRange(loc, ship.getLocation(), ship.getShield().getRadius() +
                            splitDistance)) {
                        isInShields = ship.getShield().isWithinArc(loc);
                    }
                }

                if (!isInShields) {
                    if (!MathUtils.isWithinRange(loc, ship.getLocation(), ship.getCollisionRadius() + splitDistance)) {
                        continue;
                    }
                }

                if (isInShields) {
                    if (CollisionUtils.getCollides(loc, projection, ship.getLocation(), ship.getShield().getRadius())) {
                        finalList.add(ship);
                    }
                } else if (CollisionUtils.getCollides(loc, projection, ship.getLocation(), ship.getCollisionRadius())) {
                    Vector2f point = CollisionUtils.getCollisionPoint(loc, projection, ship);
                    if (point != null && MathUtils.getDistance(loc, point) <= splitDistance) {
                        finalList.add(ship);
                    }
                }

            }

            ShipAPI closest = null;
            float closestSquareDistance = Float.MAX_VALUE;
            listSize = finalList.size();
            for (int j = 0; j < listSize; j++) {
                ShipAPI ship = finalList.get(j);
                float squareDistance = MathUtils.getDistanceSquared(loc, ship.getLocation());
                if (squareDistance < closestSquareDistance) {
                    closestSquareDistance = squareDistance;
                    closest = ship;
                }
            }

            if (closest != null) {
                if ((closest.getOwner() == 1 || closest.getOwner() == 0) && closest.getOwner() != proj.getOwner()) {
                    shouldSplit = true;
                }
            }
        }

        if (shouldSplit) {
            Vector2f scaledVel = new Vector2f(vel);
            scaledVel.scale(0.5f);
            engine.spawnExplosion(loc, scaledVel, detonateColor, detonateSize, detonateDuration);
            Global.getSoundPlayer().playSound(detonateSound, 0.25f, 1f, loc, scaledVel);
            float forceMultiplier = vel.length() / speed;

            for (int j = 0; j < particleCount; j++) {
                Vector2f randomVel = MathUtils.getRandomPointOnCircumference(null, speedScalar / rangeScalar *
                        forceMultiplier *
                        MathUtils.getRandomNumberInRange(
                                splitForceMin, splitForceMax));
                randomVel.scale((float) Math.random() + 0.75f);
                Vector2f.add(vel, randomVel, randomVel);
                randomVel.scale((float) Math.random() + 0.25f);
                engine.addHitParticle(loc, randomVel, (float) Math.random() * 2f + 6f, 1f, ((float) Math.random() *
                                0.75f + 1.25f) *
                                detonateDuration,
                        particleColor);
            }

            Vector2f defaultVel = new Vector2f(defaultSpeed * speedScalar, 0f);
            VectorUtils.rotate(defaultVel, proj.getFacing(), defaultVel);
            Vector2f actualVel = new Vector2f();
            for (int j = 0; j < submunitions; j++) {
                Vector2f randomVel = MathUtils.getRandomPointOnCircumference(null, speedScalar / rangeScalar *
                        forceMultiplier *
                        MathUtils.getRandomNumberInRange(
                                splitForceMin, splitForceMax));
                Vector2f.add(defaultVel, randomVel, actualVel);
                Vector2f.add(vel, randomVel, randomVel);
                DamagingProjectileAPI subProj = (DamagingProjectileAPI) engine.spawnProjectile(proj.getSource(),
                        proj.getWeapon(),
                        newSpec, loc,
                        VectorUtils.getFacing(
                                actualVel),
                        randomVel);
                Vector2f subVel = subProj.getVelocity();
                Vector2f.sub(subVel, defaultVel, subVel);
            }
            kaneProjectileSet.remove(proj);
            engine.removeEntity(proj);
        }
    }

    private void processMine(DamagingProjectileAPI proj) {
        if (proj.isFading() || proj.didDamage()) {
            // There is a potential memory leak if a script removes these projectiles directly
            // However, this is unlikely, and the set get flushed with each combat
            // As a result, we can safely ignore this potential problem (it would be fairly CPU-intensive to solve)
            mineProjectileSet.remove(proj);
            return;
        }

        if (!mineProjectileSet.contains(proj)) {
            mineProjectileSet.add(proj);
        }

        boolean fuseTriggered = false;

        List<ShipAPI> closeEnemies = AIUtils.getNearbyEnemies(proj, 200f);
        if (!closeEnemies.isEmpty()) {
            fuseTriggered = true;
        }

        if (fuseTriggered) {
            engine.spawnExplosion(proj.getLocation(), proj.getVelocity(), MINE_DETONATION_COLOR, MINE_DETONATION_SIZE, MINE_DETONATION_DURATION);
            Global.getSoundPlayer().playSound(MINE_DETONATION_SOUND, 1f, 1f, proj.getLocation(), proj.getVelocity());
            mineProjectileSet.remove(proj);
            return;
        }
    }

    private void processProjectileFlicker(DamagingProjectileAPI proj) {
        if (proj.isFading() || proj.didDamage()) {
            // There is a potential memory leak if a script removes these projectiles directly
            // However, this is unlikely, and the set get flushed with each combat
            // As a result, we can safely ignore this potential problem (it would be fairly CPU-intensive to solve)
            trailingProjectileSet.remove(proj);
            return;
        }

        if (!trailingProjectileSet.contains(proj)) {
            trailingProjectileSet.add(proj);
        }

        //TODO
    }

    private void processProjectileTrail(DamagingProjectileAPI proj) {
        if (!TRAIL_SPRITES.keySet().contains(proj.getProjectileSpecId())) {
            return;
        }

        if (this.projectileTrailIDs.get(proj) == null) {
            this.projectileTrailIDs.put(proj, MagicTrailPlugin.getUniqueID());
        }

        String specID = proj.getProjectileSpecId();
        SpriteAPI spriteToUse = Global.getSettings().getSprite("trails", TRAIL_SPRITES.get(specID));

        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * 1f, (float) Math.sin(Math.toRadians(proj.getFacing())) * 1f);
        Vector2f spawnPosition = new Vector2f(offsetPoint.x + proj.getLocation().x, offsetPoint.y + proj.getLocation().y);

        MagicTrailPlugin.AddTrailMemberAdvanced(
            proj,
            this.projectileTrailIDs.get(proj),
            spriteToUse,
            spawnPosition,
            0f,
            0f,
            proj.getFacing() - 180f,
            0f,
            0f,
            START_SIZE.get(specID),
            END_SIZE.get(specID),
            TRAIL_COLOR.get(specID),
            FADE_COLOR.get(specID),
            TRAIL_OPACITY.get(specID),
            DURATION_IN.get(specID),
            DURATION_MAIN.get(specID),
            DURATION_FADE.get(specID),
            GL_SRC_ALPHA,
            GL_ONE,
            LOOP_LENGTH.get(specID),
            SCROLL_SPEED.get(specID),
            new Vector2f(0f, 0f),
            null
        );

        if (this.projectileSecondaryTrailIDs.get(proj) == null) {
            this.projectileSecondaryTrailIDs.put(proj, MagicTrailPlugin.getUniqueID());
        }

        float opacityMult = 1f;
        if (proj.isFading()) {
            opacityMult = proj.getDamageAmount() / proj.getBaseDamageAmount();
        }

        Vector2f velocity = new Vector2f(proj.getVelocity());
        if (velocity.length() < 0.1f && proj.getSource() != null) {
            velocity = new Vector2f(proj.getSource().getVelocity());
        }

        Vector2f projectileVelocity = VectorUtils.rotate(velocity, -proj.getFacing());
        Vector2f baseVelocity = new Vector2f(0f, projectileVelocity.getY());
        Vector2f lateralVelocity = VectorUtils.rotate(baseVelocity, proj.getFacing());

        spriteToUse = Global.getSettings().getSprite("trails", "fds_jagged_trail");

        MagicTrailPlugin.AddTrailMemberAdvanced(
            proj,
            projectileSecondaryTrailIDs.get(proj),
            spriteToUse,
            spawnPosition,
            0f,
            MathUtils.getRandomNumberInRange(0f, 200f),
            proj.getFacing() - 180f,
            0f,
            MathUtils.getRandomNumberInRange(-400f, 400f),
            START_SIZE.get(specID),
            END_SIZE.get(specID),
            FADE_COLOR.get(specID),
            FADE_COLOR.get(specID),
            0.3f * opacityMult,
            DURATION_IN.get(specID),
            0.2f,
            DURATION_FADE.get(specID) + 0.2f,
            GL_SRC_ALPHA,
            GL_ONE,
            LOOP_LENGTH.get(specID),
            SCROLL_SPEED.get(specID),
            lateralVelocity,
            null
        );

    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
        this.projectileTrailIDs.clear();
        this.projectileSecondaryTrailIDs.clear();
        Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
    }

    @Override
    public void renderInWorldCoords(ViewportAPI view) {
        if (engine == null || engine.isPaused()) {
            return;
        }

        if (trailingProjectileSet != null && !trailingProjectileSet.isEmpty()) {
            float amount = (engine.isPaused() ? 0f : engine.getElapsedInLastFrame());
            glEnable(GL_TEXTURE_2D);
            for (Iterator<DamagingProjectileAPI> iter = trailingProjectileSet.iterator(); iter.hasNext();) {
                DamagingProjectileAPI proj = iter.next();
                if (proj.didDamage() || !engine.isEntityInPlay(proj)) {
                    iter.remove();
                    continue;
                }

                // Advance effect
//                entry.getValue().advanceAndRender(amount);
            }
        }
    }

    private static final class LocalData {

        final Set<DamagingProjectileAPI> kaneProjectileSet = new HashSet<>(100);
        final Set<DamagingProjectileAPI> mineProjectileSet = new HashSet<>(100);
        final Set<DamagingProjectileAPI> trailingProjectileSet = new HashSet<>(100);
    }
}
